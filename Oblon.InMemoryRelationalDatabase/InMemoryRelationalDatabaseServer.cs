﻿using Oblon.RelationalDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PostSharp.Patterns.Threading;
using PostSharp.Patterns.Model;

namespace Oblon.InMemoryRelationalDatabase
{
    [ReaderWriterSynchronized]
    public class InMemoryRelationalDatabaseServer : RelationalDatabaseServer
    {
        protected long currentID;

        [Reference]
        protected Dictionary<long, Row> rowsByID;

        [Reference]
        protected Dictionary<string, Row> rowsByKey;

        [Reference]
        protected Dictionary<long, Dictionary<long, Row>> rowsByParentID;


        [Reader]
        public Row GetRowByID(long id)
        {
            return rowsByID[id];
        }

        [Reader]
        public Row GetRowByKey(string key)
        {
            return rowsByKey.ContainsKey(key) ? rowsByKey[key] : null;
        }

        [Reader]
        public Row GetRowByKeyAndType(string key, RowTypes type)
        {
            var row = rowsByKey.ContainsKey(key) ? rowsByKey[key] : null;
            return row != null ? (row.RowType == type ? row : null) : null;
        }

        [Reader]
        public IEnumerable<Row> GetRowsByParentID(long parentID)
        {
            if (rowsByParentID.ContainsKey(parentID))
                return rowsByParentID[parentID].Select(r => r.Value).OrderBy(r2 => r2.ID);
            else
                return new List<Row>();
        }

        [Writer]
        public long AddRow(Row row)
        {
            row.ID = ++currentID;
            AddRowToInternalStructures(row);
            return row.ID.Value;
        }

        private void AddRowToInternalStructures(Row row)
        {
            AddRowToRowsByID(row);
            AddRowToRowsByKey(row);
            AddRowToRowsByParentIDIfHasOne(row);
        }

        private void AddRowToRowsByID(Row row)
        {
            rowsByID.Add(row.ID.Value, row);
        }

        private void AddRowToRowsByKey(Row row)
        {
            if (rowsByKey.ContainsKey(row.Key))
                throw new UniqueKeyVioliation();
            rowsByKey.Add(row.Key, row);
        }

        private void AddRowToRowsByParentIDIfHasOne(Row row)
        {
            if (row.ParentID.HasValue)
                AddRowToRowsByParentID(row);
        }

        private void AddRowToRowsByParentID(Row row)
        {
            if (!rowsByParentID.ContainsKey(row.ParentID.Value))
                rowsByParentID.Add(row.ParentID.Value, new Dictionary<long, Row>());
            rowsByParentID[row.ParentID.Value].Add(row.ID.Value, row);
        }

        [Writer]
        public void UpdateRowByID(long id, object value)
        {
            rowsByID[id].Value = value;
        }

        [Writer]
        public long GetIncrementedRowValueByID(long id)
        {
            rowsByID[id].Value = (long)rowsByID[id].Value + 1;
            return (long)rowsByID[id].Value;
        }

        [Writer]
        public void DeleteRowByID(long id)
        {
            var row = rowsByID[id];
            if (row != null)
                DeleteRow(row);
        }        

        private void DeleteRow(Row row)
        {
            rowsByKey.Remove(row.Key);
            if (row.ParentID.HasValue)
                rowsByParentID[row.ParentID.Value].Remove(row.ID.Value);
            rowsByID.Remove(row.ID.Value);
        }

        public InMemoryRelationalDatabaseServer()
        {
            rowsByID = new Dictionary<long, Row>();
            rowsByKey = new Dictionary<string, Row>();
            rowsByParentID = new Dictionary<long, Dictionary<long, Row>>();
            currentID = 0;
        }
    }
}