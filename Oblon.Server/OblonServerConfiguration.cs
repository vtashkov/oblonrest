﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.Server
{
    public interface OblonServerConfiguration
    {
         string Url { get; set; }

         string Database { get; set; }

         string ConnectionString { get; set; }

         IList<OblonUser> Users { get; set; }
    }
}
