﻿using Oblon.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.Server
{
    public abstract class OblonServer : IDisposable
    {
        protected OblonEngine engine;

        public OblonServerConfiguration Configuration { get; set; }

        public abstract void Start();

        public abstract void Stop();

        public OblonServer(OblonEngine engine)
        {
            this.engine = engine;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Stop();
            }
        }
    }
}
