﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.Server
{
    public class OblonUser
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public IList<string> DeniedUrls { get; set; }

        public IList<string> AllowedUrls { get; set; }

        public OblonUser()
        {
            DeniedUrls = new List<string>();
            AllowedUrls = new List<string>();
        }
    }
}
