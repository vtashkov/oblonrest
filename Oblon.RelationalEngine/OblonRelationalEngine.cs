﻿using Oblon.Engine;
using Oblon.RelationalDatabase;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Patterns.Contracts;
using LinqToQuerystring;

namespace Oblon.RelationalEngine
{
    public class OblonRelationalEngine : OblonEngine
    {
        private RelationalDatabaseServer database;

        public dynamic Select([NotNull] string key, [Required] SelectModifier modifier)
        {
            var row = database.GetRowByKey(key);
            return row != null ? GetResource(row, modifier) : null;
        }

        private dynamic GetResource(Row row, SelectModifier modifier)
        {
            switch (row.RowType)
            {
                case RowTypes.Collection: return GetCollection(row.ID.Value, modifier);
                case RowTypes.Object: return GetObject(row.ID.Value, modifier);
                default: throw new InvalidResourceKey();
            }
        }

        private IEnumerable<dynamic> GetCollection(long rowID, SelectModifier modifier)
        {
            var collection = new List<dynamic>();
            foreach (var objectRow in database.GetRowsByParentID(rowID))
                collection.Add(GetObject(objectRow.ID.Value, modifier));
            return !string.IsNullOrWhiteSpace(modifier.LinqQuery) ? QueryCollection(collection, modifier.LinqQuery) : collection;
        }

        private IEnumerable<dynamic> QueryCollection(IEnumerable<dynamic> collection, string query)
        {
            return collection.Select(o => o as IDictionary<string, object>).AsQueryable().LinqToQuerystring(query);
        }

        private dynamic GetObject(long rowID, SelectModifier modifier)
        {
            IDictionary<string, object> obj = new ExpandoObject();
            foreach (var row in database.GetRowsByParentID(rowID))
                AddProperty(modifier, row, obj);
            return obj;
        }

        private void AddProperty(SelectModifier modifier, Row row, IDictionary<string, object> obj)
        {
            if (row.RowType == RowTypes.Property && modifier.ShouldIncludeProperty(row.LastKey))
                obj.Add(row.LastKey, row.Value);
            else if (row.RowType != RowTypes.Property && modifier.ShouldIncludeNestedResource(row.LastKey))
                obj.Add(row.LastKey, GetResource(row, new SelectEverythingModifier()));
        }

        public long Insert([Required] string key, [Required] IDictionary<string, object> obj)
        {
            AddObjectsInCollection(key, null, new List<IDictionary<string, object>>() { obj });
            return Convert.ToInt64(obj["id"]);
        }

        public void AddObjectsInCollection(string collectionKey, long? parentID, IEnumerable<dynamic> objects)
        {
            var collectionRow = AddOrGetCollectionRow(collectionKey, parentID);
            foreach (var obj in objects)
                AddObjectInCollection(collectionRow, obj);
        }

        private Row AddOrGetCollectionRow(string key, long? parentID)
        {
            return database.GetRowByKeyAndType(key, RowTypes.Collection) ?? AddCollection(key, parentID);
        }

        private Row AddCollection(string key, long? parentID)
        {
            EnsureCorrectKey(key);
            var collectionRow = new CollectionRow(key) { ParentID = parentID };
            database.AddRow(collectionRow);
            return collectionRow;
        }

        private void EnsureCorrectKey(string key)
        {
            if (!IsRootKey(key) && !IsObject(key.ParentKey()))
                throw new InvalidResourceKey();
        }

        private bool IsRootKey(string key)
        {
            var keySegments = key.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            return keySegments.Count() == 1;
        }

        private bool IsObject(string key)
        {
            var row = database.GetRowByKeyAndType(key, RowTypes.Object);
            return row != null;
        }

        private void AddObjectInCollection(Row collectionRow, IDictionary<string, object> obj)
        {
            SetObjectIDIfNotExists(collectionRow, obj);
            AddObject(collectionRow.ID.Value, collectionRow.Key + "/" + obj["id"], obj);
        }

        private void SetObjectIDIfNotExists(Row collectionRow, IDictionary<string, object> obj)
        {
            if (!obj.ContainsKey("id"))
                obj.AddAsFirst("id", GetNextAvailableIDForCollection(collectionRow));
        }

        protected long GetNextAvailableIDForCollection(Row collectionRow)
        {
            long id;
            do
                id = database.GetIncrementedRowValueByID(collectionRow.ID.Value);
            while (database.GetRowByKey(collectionRow.Key + "/" + id) != null);
            return id;
        }
        private void AddObject(long parentID, string key, IDictionary<string, object> obj)
        {
            try
            {
                AddObjectWithExceptions(parentID, key, obj);
            }
            catch (UniqueKeyVioliation)
            {
                throw new DuplicatedObjectIDException();
            }
        }

        private void AddObjectWithExceptions(long parentID, string key, IDictionary<string, object> obj)
        {
            var newObjectRow = AddObjectRow(parentID, key);
            AddPropertiesToObject(newObjectRow, obj);
        }

        private Row AddObjectRow(long parentID, string key)
        {
            var row = new Row { ParentID = parentID, Key = key, RowType = RowTypes.Object };
            database.AddRow(row);
            return row;
        }

        private void AddPropertiesToObject(Row objectRow, IDictionary<string, object> properties)
        {
            foreach (var propertyName in properties.Keys)
                AddToObject(objectRow, propertyName, properties[propertyName]);
        }

        private void AddToObject(Row objectRow, string propertyName, object value)
        {
            if (value is IDictionary<string, object>)
                AddObject(objectRow.ID.Value, objectRow.Key + "/" + propertyName, value as IDictionary<string, object>);
            else if (value is IEnumerable<dynamic>)
                AddObjectsInCollection(objectRow.Key + "/" + propertyName, objectRow.ID, value as IEnumerable<dynamic>);
            else
                AddPropertyToObject(objectRow, propertyName, value);
        }

        private void AddPropertyToObject(Row objectRow, string propertyName, object value)
        {
            var newPropertyRow = new Row()
            {
                ParentID = objectRow.ID.Value,
                Key = objectRow.Key + "/" + propertyName,
                Value = value,
                RowType = RowTypes.Property
            };
            database.AddRow(newPropertyRow);
        }

        public void Update([Required] string key, [Required] IDictionary<string, object> obj)
        {
            var objRow = GetObjectRowOrThrowException(key);
            OverwriteObjectIDFromTheKeyIfObjectIsInCollection(objRow, obj);
            UpdateObject(objRow, obj);
        }

        protected void UpdateObject(Row objRow, IDictionary<string, object> obj)
        {
            RemoveOldProperties(objRow, obj);
            PartiallyUpdateObject(objRow, obj);
        }

        public void PartiallyUpdate([Required] string key, [Required] IDictionary<string, object> obj)
        {

            var objRow = GetObjectRowOrThrowException(key);
            OverwriteObjectIDFromTheKeyIfObjectIsInCollection(objRow, obj);
            PartiallyUpdateObject(objRow, obj);
        }

        protected void PartiallyUpdateObject(Row objRow, IDictionary<string, object> obj)
        {
            UpdateProperties(objRow, obj);
            AddNewProperties(objRow, obj);
        }

        private Row GetObjectRowOrThrowException(string key)
        {
            var row = database.GetRowByKeyAndType(key, RowTypes.Object);
            if (row == null)
                throw new ResourceNotFound();
            return row;
        }

        private void OverwriteObjectIDFromTheKeyIfObjectIsInCollection(Row objRow, IDictionary<string, object> obj)
        {
            var parentRow = database.GetRowByID(objRow.ParentID.Value);
            if (parentRow.RowType == RowTypes.Collection)
                OverwriteObjectIDFromTheKey(objRow.Key, obj);
        }

        private void OverwriteObjectIDFromTheKey(string key, IDictionary<string, object> obj)
        {
            obj["id"] = Convert.ToInt64(key.Split('/').Last());
        }

        private void RemoveOldProperties(Row objRow, IDictionary<string, object> obj)
        {
            var propertiesForRemove = GetOldProperties(objRow, obj);
            foreach (var propertyRow in propertiesForRemove)
                DeleteCascade(propertyRow.ID.Value);
        }

        private IEnumerable<Row> GetOldProperties(Row objRow, IDictionary<string, object> obj)
        {
            IEnumerable<Row> propertyRows = GetObjectPropertyRows(objRow);
            var propertiesForRemove = propertyRows.Where(p => !obj.Keys.Contains(p.LastKey));
            return propertiesForRemove;
        }

        private void UpdateProperties(Row objRow, IDictionary<string, object> obj)
        {
            var propertiesForUpdate = GetExistingProperties(objRow, obj);
            foreach (var propertyRow in propertiesForUpdate)
                UpdateProperty(obj, propertyRow);
        }

        private List<Row> GetExistingProperties(Row objRow, IDictionary<string, object> obj)
        {
            IEnumerable<Row> propertyRows = GetObjectPropertyRows(objRow);
            var propertiesForUpdate = propertyRows.Where(p => obj.Keys.Contains(p.LastKey)).ToList();
            return propertiesForUpdate;
        }

        private IEnumerable<Row> GetObjectPropertyRows(Row objRow)
        {
            return database.GetRowsByParentID(objRow.ID.Value);
        }

        private void UpdateProperty(IDictionary<string, object> obj, Row propertyRow)
        {
            if (propertyRow.RowType == RowTypes.Property)
                database.UpdateRowByID(propertyRow.ID.Value, obj[propertyRow.LastKey]);
            else if (propertyRow.RowType == RowTypes.Object)
                UpdateObject(propertyRow, obj[propertyRow.LastKey] as IDictionary<string, object>);
            else if (propertyRow.RowType == RowTypes.Collection)
                ReplaceAllObjectsInCollection(obj, propertyRow);
        }

        private void ReplaceAllObjectsInCollection(IDictionary<string, object> obj, Row propertyRow)
        {
            DeleteAllObjectsInCollection(propertyRow.ID.Value);
            foreach (var newObj in obj[propertyRow.LastKey] as IEnumerable<dynamic>)
                AddObjectInCollection(propertyRow, newObj);
        }

        private void DeleteAllObjectsInCollection(long collectionRowID)
        {
            var oldObjects = database.GetRowsByParentID(collectionRowID);
            foreach (var oldObj in oldObjects)
                DeleteCascade(oldObj.ID.Value);
        }

        private void AddNewProperties(Row objRow, IDictionary<string, object> obj)
        {
            var propertiesForAdd = GetNewProperties(obj, objRow.ID.Value);
            AddPropertiesToObject(objRow, propertiesForAdd);
        }

        private IDictionary<string, object> GetNewProperties(IDictionary<string, object> obj, long objRowId)
        {
            var propertyRows = database.GetRowsByParentID(objRowId).Select(pr => pr.LastKey);
            return obj.Where(o => !propertyRows.Contains(o.Key)).ToDictionary(p => p.Key, p => p.Value);
        }

        public void Delete([Required] string key)
        {
            var row = database.GetRowByKey(key);
            if (row != null)
                DeleteIfCollectionOrObject(row);
        }

        private void DeleteIfCollectionOrObject(Row row)
        {
            if (row.RowType == RowTypes.Object || row.RowType == RowTypes.Collection)
                DeleteCascade(row.ID.Value);
        }

        private void DeleteCascade(long rowID)
        {
            var childRows = database.GetRowsByParentID(rowID);
            foreach (var childRow in childRows)
                DeleteCascade(childRow.ID.Value);
            database.DeleteRowByID(rowID);
        }

        public OblonRelationalEngine(RelationalDatabaseServer database)
        {
            this.database = database;
        }
    }
}