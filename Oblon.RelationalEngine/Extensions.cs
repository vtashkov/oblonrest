﻿using Oblon.Engine;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RelationalEngine
{
    static class Extensions
    {
        public static void AddAsFirst(this IDictionary<string, object> obj, string key, object value)
        {
            var oldObj = obj.ToDictionary(entry => entry.Key, entry => entry.Value);
            obj.Clear();
            obj[key] = value;
            foreach (var k in oldObj.Keys)
                obj[k] = oldObj[k];
        }

        public static string ParentKey(this string key)
        {
            return key.Substring(0, key.LastIndexOf("/"));
        }
    }
}
