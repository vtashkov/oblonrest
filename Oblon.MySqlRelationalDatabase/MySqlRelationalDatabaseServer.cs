﻿using MySql.Data.MySqlClient;
using Oblon.RelationalDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.MySqlRelationalDatabase
{
    public class MySqlRelationalDatabaseServer : RelationalDatabaseServer
    {
        private string connectionString;

        public Row GetRowByID(long id)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand("SELECT * FROM OblonData WHERE ID = @ID", conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@ID", id));
                    var reader = cmd.ExecuteReader();
                    reader.Read();
                    return GetRowFromReader(reader);
                }
            }
        }

        public Row GetRowByKey(string key)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand("SELECT * FROM OblonData WHERE `Key` = @Key", conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@Key", key));
                    var reader = cmd.ExecuteReader();
                    return reader.Read() ? GetRowFromReader(reader) : null;
                }
            }
        }

        public Row GetRowByKeyAndType(string key, RowTypes type)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand("SELECT * FROM OblonData WHERE `Key` = @Key AND RowType = @RowType", conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@Key", key));
                    cmd.Parameters.Add(new MySqlParameter("@RowType", type.ToString()));
                    var reader = cmd.ExecuteReader();
                    return reader.Read() ? GetRowFromReader(reader) : null;
                }
            }
        }

        public IEnumerable<Row> GetRowsByParentID(long parentID)
        {
            var rows = new List<Row>();
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand("SELECT * FROM OblonData WHERE ParentID = @ParentID", conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@ParentID", parentID));
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        rows.Add(GetRowFromReader(reader));
                    }
                }
            }
            return rows;
        }

        public long AddRow(Row row)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand(@"INSERT INTO OblonData (RowType, ParentID, `Key`, ValueType, Value) VALUES (@RowType, @ParentID, @Key, @ValueType, @Value); SELECT last_insert_id();", conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@RowType", row.RowType.ToString()));
                    cmd.Parameters.Add(new MySqlParameter("@ParentID", row.ParentID));
                    cmd.Parameters.Add(new MySqlParameter("@Key", row.Key));
                    cmd.Parameters.Add(new MySqlParameter("@ValueType", row.ValueType.HasValue ? row.ValueType.ToString() : null));
                    cmd.Parameters.Add(new MySqlParameter("@Value", row.Value));
                    row.ID = Convert.ToInt64(cmd.ExecuteScalar());
                    return row.ID.Value;
                }
            }
        }

        public void UpdateRowByID(long id, object value)
        {
            throw new NotImplementedException();
        }

        public long GetIncrementedRowValueByID(long id)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand(@"UPDATE OblonData SET Value = Value + 1 WHERE ID = @ID; SELECT Value FROM OblonData WHERE ID = @ID;", conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@ID", id));
                    return Convert.ToInt64(cmd.ExecuteScalar());
                }
            }
        }

        public void DeleteRowByID(long id)
        {
            throw new NotImplementedException();
        }

        private void CreateMasterTableIfNotExists()
        {
            using(MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand("SELECT CASE WHEN EXISTS((SELECT * FROM information_schema.tables WHERE table_name = 'OblonData')) THEN 1 ELSE 0 END", conn))
                {
                    var masterTableExists = (long)cmd.ExecuteScalar() == 1;
                    if (!masterTableExists)
                        CreateMasterTable();
                }
            }
        }

        private void CreateMasterTable()
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand(@"CREATE TABLE OblonData (
                                                                                ID BIGINT NOT NULL AUTO_INCREMENT,
                                                                                RowType ENUM('Collection', 'Object', 'Property') NOT NULL,
                                                                                ParentID BIGINT,
                                                                                `Key` VARCHAR(3000),
                                                                                ValueType ENUM('Alphanumeric', 'Integer', 'Decimal', 'Boolean'),
                                                                                Value LONGTEXT,                                                    
                                                                                PRIMARY KEY (ID),
                                                                                INDEX(ParentID),
                                                                                INDEX(`Key`),
                                                                                INDEX(`Key`, RowType)
                                                                           )
                                                   ", conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private static Row GetRowFromReader(MySqlDataReader reader)
        {
            Row row = new Row();
            row.ID = (long)reader["ID"];
            row.RowType = (RowTypes)Enum.Parse(typeof(RowTypes), reader["RowType"].ToString());
            row.ParentID = reader["ParentID"] != DBNull.Value ? (long?)reader["ParentID"] : null;
            row.Key = reader["Key"].ToString();
            row.ValueType = reader["ValueType"] != DBNull.Value ? (ValueTypes?)Enum.Parse(typeof(ValueTypes), reader["ValueType"].ToString()) : null;
            row.Value = reader["Value"].ToString();
            return row;
        }

        public MySqlRelationalDatabaseServer(string connectionString)
        {
            this.connectionString = connectionString;
            CreateMasterTableIfNotExists();
        }
    }
}
