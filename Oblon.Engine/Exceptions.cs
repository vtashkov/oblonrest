﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.Engine
{
    [Serializable]
    public class InvalidResourceKey : Exception
    {
        public InvalidResourceKey()
            : base("Invalid or missing resource key")
        {

        }
    }

    [Serializable]
    public class ResourceNotFound : Exception
    {
        public ResourceNotFound()
            : base("Resource not found")
        {

        }
    }

    [Serializable]
    public class DuplicatedObjectIDException : Exception
    {
        public DuplicatedObjectIDException()
            : base("Duplicated object ID")
        { }
    }
}
