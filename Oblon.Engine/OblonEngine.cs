﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.Engine
{
    public interface OblonEngine
    {
        dynamic Select(string key, SelectModifier modifier);

        long Insert(string key, IDictionary<string, object> obj);

        void Update(string key, IDictionary<string, object> obj);

        void PartiallyUpdate(string key, IDictionary<string, object> obj);

        void Delete(string key);
    }
}
