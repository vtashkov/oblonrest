﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.Engine
{
    public class SelectModifier
    {
        public bool Full { get; set; }
        
        public string[] Fields { get; set; }

        public string[] Expand { get; set; }

        public string LinqQuery { get; set; }

        public virtual bool ShouldIncludeProperty(string key)
        {
            return Full ||
                   Fields.Length == 0 ||
                   Expand.Contains(key) ||
                   Fields.Contains(key);
        }

        public virtual bool ShouldIncludeNestedResource(string key)
        {
            return Full ||
                   Expand.Contains(key) ||
                   Fields.Contains(key);
        }

        public SelectModifier()
        {
            Full = false;
            Fields = new string[] { };
            Expand = new string[] { };
        }
    }    

    public class SelectEverythingModifier : SelectModifier
    {
        public override bool ShouldIncludeProperty(string key)
        {
            return true;
        }

        public override bool ShouldIncludeNestedResource(string key)
        {
            return true;
        }
    }
}
