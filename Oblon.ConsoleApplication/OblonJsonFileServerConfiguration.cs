﻿using Newtonsoft.Json;
using Oblon.RestServer;
using Oblon.Server;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.ConsoleApplication
{
    class OblonJsonFileServerConfiguration : OblonServerConfiguration
    {
        [JsonProperty("url", Required = Required.Always)]
        public string Url { get; set; }

        [JsonProperty("Database")]
        public string Database { get; set; }

        [JsonProperty("ConnectionString")]
        public string ConnectionString { get; set; }

        [JsonProperty("Users")]
        public IList<OblonUser> Users { get; set; }

        public static OblonJsonFileServerConfiguration LoadFile(string filename)
        {
            return JsonConvert.DeserializeObject<OblonJsonFileServerConfiguration>(File.ReadAllText(filename));
        }
    }
}
