﻿using Newtonsoft.Json;
using Oblon.Engine;
using Oblon.InMemoryRelationalDatabase;
using Oblon.MySqlRelationalDatabase;
using Oblon.RelationalDatabase;
using Oblon.RelationalEngine;
using Oblon.RestServer;
using Oblon.Server;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Oblon.ConsoleApplication
{
    class Application
    {
        private string configFileName;
        private OblonServerConfiguration configuration;
        private OblonEngine engine;
        private Authenticator authenticator;

        public void Run()
        {
            PrintInitialMessage();
            LoadConfiguration();
            TryStartWebServer();
        }

        private static void PrintInitialMessage()
        {
            Console.WriteLine("Oblon Server 0.3.1 / 2.11.2015");
        }

        private void LoadConfiguration()
        {
            LoadConfigurationFile();
            engine = new OblonRelationalEngine(GetRelationalDatabaseServer(configuration.Database, configuration.ConnectionString));
            authenticator = configuration.Users != null ? (Authenticator)new BasicAuthenticator(configuration.Users.ToDictionary(u => u.Username, u => u)) : (Authenticator)new AnonymousAuthenticator();
        }

        private RelationalDatabaseServer GetRelationalDatabaseServer(string database, string connectionString)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(database))
                    return new InMemoryRelationalDatabaseServer();
                else if (database.ToLower() == "mysql")
                    return new MySqlRelationalDatabaseServer(connectionString);
                else
                    throw new Exception("Invalid database specified in configuration: " + database);
            }
            catch (Exception ex)
            {
                PrintErrorMessage(ex);
                Environment.Exit(-1);
            }

            return null;
        }
 
        private void LoadConfigurationFile()
        {
            try
            {
                configuration = OblonJsonFileServerConfiguration.LoadFile(configFileName);
            }
            catch(FileNotFoundException)
            {
                PrintMissingConfigurationFileErrorMessage();
                Environment.Exit(-1);
            }
            catch(JsonReaderException)
            {
                PrintInvalidConfigurationFileErrorMessage();
                Environment.Exit(-1);
            }
            catch (JsonSerializationException ex)
            {
                PrintInvalidConfigurationParameterErrorMessage(ex.Message);
                Environment.Exit(-1);
            }
        }

        private void PrintMissingConfigurationFileErrorMessage()
        {
            Console.WriteLine("Missing configuration file: " + configFileName);
        }

        private void PrintInvalidConfigurationFileErrorMessage()
        {
            Console.WriteLine("Invalid configuration file - it should be correct JSON document");
        }

        private void PrintInvalidConfigurationParameterErrorMessage(string error)
        {
            Console.WriteLine("Invalid configuration parameter: " + error);
        }

        private void TryStartWebServer()
        {
            try
            {
                StartWebServer(configuration);
            }
            catch (ListenerAccessDeniedException ex)
            {
                PrintErrorMessage(ex);
                PrintAccessDeniedErrorAdvice();
            }
            catch (Exception ex)
            {
                PrintErrorMessage(ex);
            }
        }

        private void StartWebServer(OblonServerConfiguration configuration)
        {
            using (var server = new OblonOwinServer(engine, authenticator))
            {
                server.Configuration = configuration;
                server.Start();
                PrintWebServerStartingMessage(server);
                WaitUntilExitCommand();
            }
        }

        private static void PrintWebServerStartingMessage(OblonOwinServer server)
        {
            Console.WriteLine("Oblon Server started on " + server.Configuration.Url);
            Console.WriteLine("Type Ctr-D to exit");
        }

        private static void WaitUntilExitCommand()
        {
            var key = Console.ReadKey(true);
            while (((key.Modifiers & ConsoleModifiers.Control) == 0) || (key.Key != ConsoleKey.D))
                key = Console.ReadKey(true);
        }

        private static void PrintErrorMessage(Exception ex)
        {
            Console.WriteLine("ERROR: " + ex.Message);
        }

        private static void PrintAccessDeniedErrorAdvice()
        {
            Console.WriteLine("Try starting Oblon Server with Administrative rights.");
        }

        public Application(string configFileName)
        {
            this.configFileName = configFileName;
        }
    }
}
