﻿using Oblon.InMemoryRelationalDatabase;
using Oblon.RelationalEngine;
using Oblon.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            string configFileName = args.Length > 0 ? args[0] : "oblon.config";
            new Application(configFileName).Run();
        }
    }
}
