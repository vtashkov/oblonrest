﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Oblon.Engine;
using Oblon.RestServer.RequestHandlers;

namespace Oblon.RestServer
{
    class RequestHandlerFactory
    {
        private Dictionary<RequestMethod, Type> RequestMethodHandlerMap;
        private OblonEngine engine;
        private Authenticator authenticator;

        public RequestHandler GetHandler(Request request)
        {
            if (RequestMethodHandlerMap.ContainsKey(request.Method))
                return (RequestHandler)Activator.CreateInstance(RequestMethodHandlerMap[request.Method], engine, authenticator, request);
            else
                throw new InvalidRequestMethodException();
        }

        public RequestHandlerFactory(OblonEngine engine, Authenticator authenticator)
        {
            this.engine = engine;
            this.authenticator = authenticator;
            RequestMethodHandlerMap = new Dictionary<RequestMethod, Type>()
                                      {
                                          {RequestMethod.Get, typeof(GetResourceRequestHandler)},
                                          {RequestMethod.Post, typeof(InsertObjectRequestHandler)},
                                          {RequestMethod.Put, typeof(UpdateObjectRequestHandler)},
                                          {RequestMethod.Patch, typeof(PartiallyUpdateObjectRequestHandler)},
                                          {RequestMethod.Delete, typeof(DeleteResourceRequestHandler)},
                                      };
        }
    }
}
