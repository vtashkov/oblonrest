﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Oblon.RestServer
{
    public static class JsonConvertor
    {
        public static string ObjectToString(dynamic obj)
        {
            return obj != null ? JsonConvert.SerializeObject(obj) : "";
        }

        public static dynamic StringToObject(string s)
        {
            try
            {
                return StringToObjectWithoutExceptionHandling(s);
            }
            catch (Exception)
            {
                throw new InvalidBodyException();
            }
        }

        private static dynamic StringToObjectWithoutExceptionHandling(string s)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.FloatParseHandling = FloatParseHandling.Decimal;
            return JsonConvert.DeserializeObject<ExpandoObject>(s, settings);
        }
    }
}
