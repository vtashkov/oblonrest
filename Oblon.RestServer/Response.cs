﻿using System;

namespace Oblon.RestServer
{
    class Response
    {
        public int StatusCode { get; set; }

        public byte[] Body { get; set; }

        public string LocationHeader { get; set; }

        public string WWWAuthenticateHeader { get; set; }

        public string ContentEncoding { get; set; }

        public Response()
        {
            Body = new byte[] { };
        }
    }
}
