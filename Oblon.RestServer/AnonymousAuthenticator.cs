﻿using Oblon.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer
{
    public class AnonymousAuthenticator : Authenticator
    {
        public bool IsAuthenticated(string username, string password)
        {
            return true;
        }

        public bool IsAuthorized(Request request)
        {
            return true;
        }

        public AuthenticationSchemes GetAuthenticationScheme()
        {
            return AuthenticationSchemes.Anonymous;
        }
    }
}
