﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer
{
    public interface Authenticator
    {
        AuthenticationSchemes GetAuthenticationScheme();

        bool IsAuthenticated(string username, string password);

        bool IsAuthorized(Request request);
    }
}
