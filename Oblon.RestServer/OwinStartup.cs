﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Oblon.RestServer
{
    public class OwinStartup
    {
        private Authenticator authenticator;
        private OblonRestProcessor processor;

        public void Configuration(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            ConfigureAuthentication(app);            
            app.Run(context =>
            {
                return processor.Process(context.Request, context.Response);                
            });
        }

        private void ConfigureAuthentication(IAppBuilder app)
        {
            if (app.Properties.ContainsKey(typeof(HttpListener).FullName))
            {
                var listener = (HttpListener)app.Properties[typeof(HttpListener).FullName];
                listener.AuthenticationSchemeSelectorDelegate += AuthenticationSchemeSelectorDelegate;
            }
        }

        private AuthenticationSchemes AuthenticationSchemeSelectorDelegate(HttpListenerRequest httpRequest)
        {
            if (httpRequest.Headers.Get("Access-Control-Request-Method") != null)
                return AuthenticationSchemes.Anonymous;
            else
                return authenticator.GetAuthenticationScheme();
        }

        public OwinStartup(OblonRestProcessor processor, Authenticator authenticator)
        {
            this.authenticator = authenticator;
            this.processor = processor;
        }
    }
}
