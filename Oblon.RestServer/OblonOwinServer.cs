﻿using Microsoft.Owin.Hosting;
using Oblon.Engine;
using Oblon.Server;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer
{
    [ExcludeFromCodeCoverage]
    public class OblonOwinServer : OblonServer
    {
        private OwinStartup startup;
        private IDisposable webApp;

        public override void Start()
        {
            try
            {
                webApp = WebApp.Start(Configuration.Url, startup.Configuration);
            }
            catch (Exception ex)
            {
                if (ex.InnerException is HttpListenerException)
                    throw new ListenerAccessDeniedException(Configuration.Url);
                throw;
            }
        }

        public override void Stop()
        {
            if (webApp != null)
                webApp.Dispose();
        }

        public OblonOwinServer(OblonEngine engine, Authenticator authenticator)
            :base(engine)
        {
            var processor = new OblonRestProcessor(engine, authenticator);
            startup = new OwinStartup(processor, authenticator);
        }
    }
}
