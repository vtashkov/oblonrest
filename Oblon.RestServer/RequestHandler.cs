﻿using System;
using System.Collections.Generic;

using Oblon.Engine;
using System.Net;
using System.IO;
using System.IO.Compression;

namespace Oblon.RestServer
{
    abstract class RequestHandler
    {
        protected OblonEngine engine;
        protected Authenticator authenticator;
        protected Request request;

        public Response Handle()
        {
            var response = new Response();
            CheckAuthentication();
            CheckAuthorization();
            Handle(response);
            CompressIfRequested(response);
            return response;
        }

        private void CheckAuthentication()
        {
            if (!authenticator.IsAuthenticated(request.Username, request.Password))
                throw new UnauthenticatedException();
        }

        private void CheckAuthorization()
        {
            if (!authenticator.IsAuthorized(request))
                throw new UnauthorizedException();
        }

        public abstract void Handle(Response response);

        private void CompressIfRequested(Response response)
        {
            if (request.GzipRequested)
                Compress(response);
        }

        private static void Compress(Response response)
        {
            response.ContentEncoding = "gzip";
            response.Body = response.Body.GZip();
        }

        public RequestHandler(OblonEngine engine, Authenticator authenticator, Request request)
        {
            this.engine = engine;
            this.authenticator = authenticator;
            this.request = request;
        }
    }
}
