﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer
{
    public enum RequestMethod { Get, Post, Put, Patch, Delete, Trace }

    public enum RequestType { GetObject, GetCollection, InsertObject, UpdateObject, PartiallyUpdateObject, DeleteObject, DeleteCollection, Undefined }

    public class Request
    {
        public Uri Uri { get; set; }

        public RequestMethod Method { get; private set; }

        public string ResourceKey { get; private set; } 

        public string Body { get; private set; }

        public string Username { get; private set; }

        public string Password { get; private set; }

        public bool GzipRequested { get; set; }

        public Request(IOwinRequest owinRequest)
        {
            Uri = owinRequest.Uri;
            Method = GetRequestTypeFromString(owinRequest.Method);
            ResourceKey = owinRequest.Uri.LocalPath;
            Body = new StreamReader(owinRequest.Body).ReadToEnd();
            SetupUser(owinRequest);
            GzipRequested = owinRequest.Headers.ContainsKey("Accept-Encoding") && owinRequest.Headers.GetCommaSeparatedValues("Accept-Encoding").Any(x => x == "gzip");
            UncompressBodyIfEncoded(owinRequest);
        }

        private RequestMethod GetRequestTypeFromString(string reqType)
        {
            Dictionary<string, RequestMethod> requestTypeMap = new Dictionary<string, RequestMethod>()
                                                             {
                                                                 {"GET", RequestMethod.Get},
                                                                 {"POST", RequestMethod.Post},
                                                                 {"PUT", RequestMethod.Put},
                                                                 {"PATCH", RequestMethod.Patch},
                                                                 {"DELETE", RequestMethod.Delete},
                                                                 {"TRACE", RequestMethod.Trace}
                                                             };

            return requestTypeMap[reqType];
        }

        private void SetupUser(IOwinRequest owinRequest)
        {
            if (owinRequest.User != null)
            {
                HttpListenerBasicIdentity identity = (HttpListenerBasicIdentity)owinRequest.User.Identity;
                Username = identity.Name;
                Password = identity.Password;
            }
        }

        private void UncompressBodyIfEncoded(IOwinRequest owinRequest)
        {
            if (owinRequest.Headers.ContainsKey("Content-Encoding") && 
                owinRequest.Headers.GetCommaSeparatedValues("Content-Encoding").Any(x => x == "gzip") &&
                !string.IsNullOrEmpty(Body))
                Body = Body.GUnzip();
        }
    }
}
