﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oblon.Engine;

namespace Oblon.RestServer.RequestHandlers
{
    class PartiallyUpdateObjectRequestHandler : RequestHandler
    {
        public override void Handle(Response response)
        {
            dynamic obj = JsonConvertor.StringToObject(request.Body);
            engine.PartiallyUpdate(request.ResourceKey, obj);
            response.StatusCode = 200;
        }

        public PartiallyUpdateObjectRequestHandler(OblonEngine engine, Authenticator authenticator, Request request)
            : base(engine, authenticator, request)
        { }
    }
}
