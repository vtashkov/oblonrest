﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oblon.Engine;

namespace Oblon.RestServer.RequestHandlers
{
    class GetResourceRequestHandler : RequestHandler
    {
        public override void Handle(Response response)
        {
            dynamic obj = engine.Select(request.ResourceKey, new SelectModifier() { Full = true });
            response.Body = System.Text.Encoding.UTF8.GetBytes(JsonConvertor.ObjectToString(obj));
            response.StatusCode = response.Body.Length > 0 ? 200 : 404;
        }

        public GetResourceRequestHandler(OblonEngine engine, Authenticator authenticator, Request request)
            : base(engine, authenticator, request)
        { }
    }
}
