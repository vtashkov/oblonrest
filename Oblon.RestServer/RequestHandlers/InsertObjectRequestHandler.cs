﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oblon.Engine;

namespace Oblon.RestServer.RequestHandlers
{
    class InsertObjectRequestHandler : RequestHandler
    {
        public override void Handle(Response response)
        {
            dynamic obj = JsonConvertor.StringToObject(request.Body);
            var newID = engine.Insert(request.ResourceKey, obj);
            string location = new Uri(request.Uri, request.ResourceKey + "/" + newID).ToString();
            response.StatusCode = 201;
            response.LocationHeader = location;
        }

        public InsertObjectRequestHandler(OblonEngine engine, Authenticator authenticator, Request request)
            :base(engine, authenticator, request)
        { }
    }
}
