﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oblon.Engine;

namespace Oblon.RestServer.RequestHandlers
{
    class DeleteResourceRequestHandler : RequestHandler
    {
        public override void Handle(Response response)
        {
            engine.Delete(request.ResourceKey);
            response.StatusCode = 200;
        }

        public DeleteResourceRequestHandler(OblonEngine engine, Authenticator authenticator, Request request)
            : base(engine, authenticator, request)
        { }
    }
}
