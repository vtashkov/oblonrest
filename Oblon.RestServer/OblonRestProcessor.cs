﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.Owin;

using Oblon.Engine;
using Oblon.Server;
using Microsoft.Owin.Hosting;
using Owin;

namespace Oblon.RestServer
{
    public class OblonRestProcessor
    {
        private RequestHandlerFactory requestHandlerFactory;

        public Task Process(IOwinRequest owinRequest, IOwinResponse owinResponse)
        {
            Response response = HandleRequestWithExceptions(owinRequest);
            return Respond(owinResponse, response);
        }

        private Response HandleRequestWithExceptions(IOwinRequest owinRequest)
        {
            try
            {
                return HandleRequest(owinRequest);
            }
            catch (Exception ex)
            {
                return HandleError(ex);
            }
        }

        private Response HandleRequest(IOwinRequest owinRequest)
        {
            Request request = new Request(owinRequest);
            return requestHandlerFactory.GetHandler(request).Handle();
        }

        private Response HandleError(Exception ex)
        {
            var errorMap = new Dictionary<Type, int>() 
            {
                {typeof(InvalidRequestMethodException), 400},
                {typeof(InvalidBodyException), 400},
                {typeof(ArgumentNullException), 400},
                {typeof(InvalidResourceKey), 400},
                {typeof(UnauthenticatedException), 401},
                {typeof(UnauthorizedException), 401},                
                {typeof(ResourceNotFound), 404},
                {typeof(DuplicatedObjectIDException), 422}
            };
            return new Response() 
            { 
                StatusCode = errorMap.ContainsKey(ex.GetType()) ? errorMap[ex.GetType()] : 500, 
                WWWAuthenticateHeader = ex is UnauthenticatedException ? "Basic Realm=\"Oblon Server\"" : "" 
            };
        }

        private Task Respond(IOwinResponse owinResponse, Response response)
        {
            owinResponse.StatusCode = response.StatusCode;
            owinResponse.ContentType = "application/json";
            AddLocationIfAny(owinResponse, response);
            AddWWWAuthenticateIfAny(owinResponse, response);
            AddContentEncodingIfany(owinResponse, response);
            return owinResponse.WriteAsync(response.Body);
        }

        private static void AddLocationIfAny(IOwinResponse owinResponse, Response response)
        {
            if (!string.IsNullOrEmpty(response.LocationHeader))
                owinResponse.Headers.Add("Location", new string[] { response.LocationHeader });
        }

        private static void AddWWWAuthenticateIfAny(IOwinResponse owinResponse, Response response)
        {
            if (!string.IsNullOrEmpty(response.WWWAuthenticateHeader))
                owinResponse.Headers.Add("WWW-Authenticate", new string[] { response.WWWAuthenticateHeader });
        }

        private static void AddContentEncodingIfany(IOwinResponse owinResponse, Response response)
        {
            if (!string.IsNullOrEmpty(response.ContentEncoding))
                owinResponse.Headers.Add("Content-Encoding", new string[] { response.ContentEncoding });
        }

        public OblonRestProcessor(OblonEngine engine, Authenticator authenticator)          
        {
            requestHandlerFactory = new RequestHandlerFactory(engine, authenticator);
        }
    }
}