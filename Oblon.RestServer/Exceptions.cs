﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer
{
    [Serializable]
    public class InvalidRequestMethodException : Exception
    {
        public InvalidRequestMethodException()
            :base("Invalid request method")
        {

        }
    }

    [Serializable]
    public class InvalidBodyException : Exception
    {
        public InvalidBodyException()
            : base("Invalid request body - empty or non-JSON")
        {

        }
    }

    [Serializable]
    [ExcludeFromCodeCoverage]
    public class ListenerAccessDeniedException : Exception
    {
        public ListenerAccessDeniedException(string url)
            : base("Access denied for listening on " + url)
        {

        }
    }

    [Serializable]
    public class UnauthenticatedException : Exception
    {
        public UnauthenticatedException()
            : base("Invalid username or password")
        {

        }
    }

    [Serializable]
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException()
            : base("User is not authorized to perform this operation")
        {

        }
    }
}
