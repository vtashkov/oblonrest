﻿using Oblon.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer
{
    public class BasicAuthenticator : Authenticator
    {
        private IDictionary<string, OblonUser> users;

        public bool IsAuthenticated(string username, string password)
        {
            return users.Count == 0 || (users.ContainsKey(username) && users[username].Password == password);
        }

        public bool IsAuthorized(Request request)
        {
            string url = request.Method.ToString().ToUpper() + " " + request.ResourceKey;
            return users.Count == 0 ||
                   !users[request.Username].DeniedUrls.Any(u => url.StartsWith(u)) ||
                   users[request.Username].AllowedUrls.Any(u => url.StartsWith(u));
        }

        public AuthenticationSchemes GetAuthenticationScheme()
        {
            return AuthenticationSchemes.Basic;
        }

        public BasicAuthenticator(IDictionary<string, OblonUser> users)
        {
            this.users = users;
        }
    }
}
