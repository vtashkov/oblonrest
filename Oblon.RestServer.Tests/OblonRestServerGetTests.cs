﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oblon.Engine;
using Oblon.Server;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer.Tests
{
    [TestClass]
    public class OblonRestServerGetTests
    {
        private OblonEngineMock engine;
        private TestServer server;
        private dynamic selectedObject;

        [TestInitialize]
        public void Initialize()
        {
            selectedObject = new ExpandoObject();
            selectedObject.id = 5;
            selectedObject.name = "name";
            selectedObject.dec = 1.2M;
            selectedObject.boolean = false;
            selectedObject.Null = null;
            selectedObject.innerObject = new ExpandoObject();
            selectedObject.innerObject.name = "InnerObject";
            selectedObject.innerObject.level2InnerObject = new ExpandoObject();
            selectedObject.innerObject.level2InnerObject.name = "Level2InnerObject";
            dynamic innerCollectionObject = new ExpandoObject();
            innerCollectionObject.name = "innerCollectionObject";
            dynamic innerCollectionObject2 = new ExpandoObject();
            innerCollectionObject2.name = "innerCollectionObject2";
            selectedObject.innerCollection = (new List<dynamic>() { innerCollectionObject, innerCollectionObject2 } as IEnumerable<dynamic>);

            engine = new OblonEngineMock();
            var processor = new OblonRestProcessor(engine, new AuthenticatorMock());
            var startup = new OwinStartup(processor, new AuthenticatorMock());
            server = TestServer.Create(startup.Configuration);       
        }

        [TestCleanup]
        public void Cleanup()
        {
            server.Dispose();
        }

        [TestMethod]
        public void RestServerReturnsBadRequestForArgumentNullException()
        {
            engine.ExceptionToBeThrown = new ArgumentNullException();
            var response = server.HttpClient.Get("");
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsBadRequestForInvalidResourceKey()
        {
            engine.ExceptionToBeThrown = new InvalidResourceKey();
            var response = server.HttpClient.Get("");
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsNotFoundForSelectedNullNullObject()
        {
            engine.OutputObject = null;
            var response = server.HttpClient.Get("");
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsCorrectObject()
        {
            engine.OutputObject = selectedObject;
            var response = server.HttpClient.Get("");
            dynamic resultObject = response.GetContentAsObject();
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(selectedObject.id, resultObject.id.ToObject(typeof(long)));
            Assert.AreEqual(selectedObject.name, resultObject.name.ToObject(typeof(string)));
            Assert.AreEqual(selectedObject.dec, resultObject.dec.ToObject(typeof(decimal)));
            Assert.AreEqual(selectedObject.boolean, resultObject.boolean.ToObject(typeof(bool)));
            Assert.AreEqual(selectedObject.Null, resultObject.Null.ToObject(typeof(string)));
        }

        [TestMethod]
        public void RestServerReturnsCorrectInnerObject()
        {
            engine.OutputObject = selectedObject;
            var response = server.HttpClient.Get("");
            dynamic resultObject = response.GetContentAsObject();
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(selectedObject.innerObject.name, resultObject.innerObject.name.ToObject(typeof(string)));
            Assert.AreEqual(selectedObject.innerObject.level2InnerObject.name, resultObject.innerObject.level2InnerObject.name.ToObject(typeof(string)));
        }

        [TestMethod]
        public void RestServerReturnsCorrectInnerCollections()
        {
            engine.OutputObject = selectedObject;
            var response = server.HttpClient.Get("");
            dynamic resultObject = response.GetContentAsObject();
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(selectedObject.innerCollection[0].name, resultObject.innerCollection[0].name.ToObject(typeof(string)));
            Assert.AreEqual(selectedObject.innerCollection[1].name, resultObject.innerCollection[1].name.ToObject(typeof(string)));
        }
    }
}