﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer.Tests
{
    [TestClass]
    public class OblonRestServerDeleteTests
    {
        private OblonEngineMock engine;
        private TestServer server;

        [TestInitialize]
        public void Initialize()
        {
            engine = new OblonEngineMock();
            var processor = new OblonRestProcessor(engine, new AuthenticatorMock());
            var startup = new OwinStartup(processor, new AuthenticatorMock());
            server = TestServer.Create(startup.Configuration);
        }

        [TestCleanup]
        public void Cleanup()
        {
            server.Dispose();
        }

        [TestMethod]
        public void RestServerReturnsBadRequestOnArgumentNullException()
        {
            engine.ExceptionToBeThrown = new ArgumentNullException();
            var response = server.HttpClient.Delete("");
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsOKOnDelete()
        {
            var response = server.HttpClient.Delete("");
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
