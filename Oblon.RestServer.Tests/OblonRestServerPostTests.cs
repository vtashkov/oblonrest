﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Oblon.Engine;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer.Tests
{
    [TestClass]
    public class OblonRestServerPostTests
    {
        private OblonEngineMock engine;
        private TestServer server;
        private dynamic newObj;        

        [TestInitialize]
        public void Initialize()
        {
            newObj = new ExpandoObject();
            newObj.id = 5;
            newObj.name = "name";
            newObj.dec = 1.1M;
            newObj.boolean = true;
            newObj.Null = null;
            newObj.innerObject = new ExpandoObject();
            newObj.innerObject.name = "InnerObject";
            newObj.innerObject.level2InnerObject = new ExpandoObject();
            newObj.innerObject.level2InnerObject.name = "Level2InnerObject";
            dynamic innerCollectionObject = new ExpandoObject();
            innerCollectionObject.name = "innerCollectionObject";
            dynamic innerCollectionObject2 = new ExpandoObject();
            innerCollectionObject2.name = "innerCollectionObject2";
            newObj.innerCollection = (new List<dynamic>() { innerCollectionObject, innerCollectionObject2 } as IEnumerable<dynamic>);

            engine = new OblonEngineMock();
            var processor = new OblonRestProcessor(engine, new AuthenticatorMock());
            var startup = new OwinStartup(processor, new AuthenticatorMock());
            server = TestServer.Create(startup.Configuration);
        }

        [TestCleanup]
        public void Cleanup()
        {
            server.Dispose();
        }

        [TestMethod]
        public void RestServerReturnsBadRequestOnArgumentNullException()
        {
            engine.ExceptionToBeThrown = new ArgumentNullException();
            var response = server.HttpClient.Post("", (object)newObj);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsBadRequestOnInvalidResourceKey()
        {
            engine.ExceptionToBeThrown = new InvalidResourceKey();
            var response = server.HttpClient.Post("", (object)newObj);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsBadRequestOnPostWithNonJSONBody()
        {
            var response = server.HttpClient.PostPlain("", "nonJSON");
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsUnprocessableEntityOnDuplicatedID()
        {
            engine.ExceptionToBeThrown = new DuplicatedObjectIDException();
            var response = server.HttpClient.Post("", (object)newObj);
            Assert.AreEqual((HttpStatusCode)422, response.StatusCode);
        }

        [TestMethod]
        public void RestServerInsertsCorrectObject()
        {
            var response = server.HttpClient.Post("", (object)newObj);
            dynamic obj = engine.InputObject;
            Assert.AreEqual(newObj.name, obj.name);
            Assert.AreEqual(newObj.dec, obj.dec);
            Assert.AreEqual(newObj.boolean, obj.boolean);
            Assert.AreEqual(newObj.Null, obj.Null);
        }

        [TestMethod]
        public void RestServerReturnsCreatedOnCorrectPost()
        {
            var response = server.HttpClient.Post("", (object)newObj);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsLocationHeaderOnCorrectPost()
        {
            var response = server.HttpClient.Post("/collection1", (object)newObj);
            Assert.AreEqual("/collection1/" + newObj.id, response.Headers.Location.LocalPath);
        }

        [TestMethod]
        public void RestServerInsertsCorrectInnerObjectsObject()
        {
            var response = server.HttpClient.Post("/collection1", (object)newObj);
            dynamic obj = engine.InputObject;
            Assert.AreEqual(newObj.innerObject.name, obj.innerObject.name);
            Assert.AreEqual(newObj.innerObject.level2InnerObject.name, obj.innerObject.level2InnerObject.name);
        }

        [TestMethod]
        public void RestServerInsertsCorrectInnerCollections()
        {
            var response = server.HttpClient.Post("/collection1", (object)newObj);
            dynamic obj = engine.InputObject;
            Assert.AreEqual(newObj.innerCollection[0].name, obj.innerCollection[0].name);
            Assert.AreEqual(newObj.innerCollection[1].name, obj.innerCollection[1].name);
        }
    }
}
