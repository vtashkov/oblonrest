﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Oblon.Engine;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer.Tests
{
    [TestClass]
    public class OblonRestServerPatchTests
    {
        private OblonEngineMock engine;
        private TestServer server;
        private dynamic changedObj;

        [TestInitialize]
        public void Initialize()
        {
            changedObj = new ExpandoObject();
            changedObj.id = 5;
            changedObj.name = "name2";
            changedObj.dec = 1.2M;
            changedObj.boolean = false;
            changedObj.Null = null;
            changedObj.innerObject = new ExpandoObject();
            changedObj.innerObject.name = "InnerObject";
            changedObj.innerObject.level2InnerObject = new ExpandoObject();
            changedObj.innerObject.level2InnerObject.name = "Level2InnerObject";
            dynamic innerCollectionObject = new ExpandoObject();
            innerCollectionObject.name = "innerCollectionObject";
            dynamic innerCollectionObject2 = new ExpandoObject();
            innerCollectionObject2.name = "innerCollectionObject2";
            changedObj.innerCollection = (new List<dynamic>() { innerCollectionObject, innerCollectionObject2 } as IEnumerable<dynamic>);

            engine = new OblonEngineMock();
            var processor = new OblonRestProcessor(engine, new AuthenticatorMock());
            var startup = new OwinStartup(processor, new AuthenticatorMock());
            server = TestServer.Create(startup.Configuration);
        }

        [TestCleanup]
        public void Cleanup()
        {
            server.Dispose();
        }

        [TestMethod]
        public void RestServerReturnsBadRequestOnArgumentNullException()
        {
            engine.ExceptionToBeThrown = new ArgumentNullException();
            var response = server.HttpClient.Patch("", (object)changedObj);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsNotFoundOnResourceNotFound()
        {
            engine.ExceptionToBeThrown = new ResourceNotFound();
            var response = server.HttpClient.Patch("", (object)changedObj);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsBadRequestOnPatchWithNonJSONBody()
        {
            var id = (long)changedObj.id;
            var response = server.HttpClient.PatchPlain("", "nonJSON");
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void RestServerUpdatesObjectOnPatch()
        {
            var id = (long)changedObj.id;
            var response = server.HttpClient.Patch("", (object)changedObj);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(changedObj.id, engine.InputObject.id);
            Assert.AreEqual(changedObj.name, engine.InputObject.name);
            Assert.AreEqual(changedObj.dec, engine.InputObject.dec);
            Assert.AreEqual(changedObj.boolean, engine.InputObject.boolean);
            Assert.AreEqual(changedObj.Null, engine.InputObject.Null);
        }

        [TestMethod]
        public void RestServerUpdatesCorrectInnerObjects()
        {
            var response = server.HttpClient.Patch("", (object)changedObj);
            dynamic obj = engine.InputObject;
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(changedObj.innerObject.name, obj.innerObject.name);
            Assert.AreEqual(changedObj.innerObject.level2InnerObject.name, obj.innerObject.level2InnerObject.name);
        }

        [TestMethod]
        public void RestServerUpdatesCorrectInnerCollections()
        {
            var response = server.HttpClient.Put("", (object)changedObj);
            dynamic obj = engine.InputObject;
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(changedObj.innerCollection[0].name, obj.innerCollection[0].name);
            Assert.AreEqual(changedObj.innerCollection[1].name, obj.innerCollection[1].name);
        }
    }
}
