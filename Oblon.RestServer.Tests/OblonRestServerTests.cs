﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer.Tests
{
    [TestClass]
    public class OblonRestServerTests
    {
        private OblonEngineMock engine;
        private TestServer server;

        [TestInitialize]
        public void Initialize()
        {
            engine = new OblonEngineMock();
            var processor = new OblonRestProcessor(engine, new AuthenticatorMock());
            var startup = new OwinStartup(processor, new AuthenticatorMock());
            server = TestServer.Create(startup.Configuration);            
        }

        [TestCleanup]
        public void Cleanup()
        {
            server.Dispose();
        }

        [TestMethod]
        public void RestServerReturnsBadRequestOnNotValidRequestMethod()
        {
            var response = server.HttpClient.SendAsync(new HttpRequestMessage(HttpMethod.Trace, "/"));
            response.Wait();
            Assert.AreEqual(HttpStatusCode.BadRequest, response.Result.StatusCode);
        }

        [TestMethod]
        public void RestServerReturnsInternalErrorOnUnrecognizedException()
        {
            engine.ExceptionToBeThrown = new Exception();
            var response = server.HttpClient.Get("");
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode);
        }
    }
}
