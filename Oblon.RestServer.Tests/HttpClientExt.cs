﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RestServer.Tests
{
    static class HttpClientExt
    {
        public static HttpResponseMessage Get(this HttpClient client, string uri)
        {
            Task<HttpResponseMessage> response = client.GetAsync(uri);
            response.Wait();
            return response.Result;
        }

        public static dynamic GetContentAsObject(this HttpResponseMessage response)
        {
            Task<string> s = response.Content.ReadAsStringAsync();
            s.Wait();
            return JsonConvert.DeserializeObject<dynamic>(s.Result);
        }

        public static HttpResponseMessage Post(this HttpClient client, string uri, object obj)
        {
            Task<HttpResponseMessage> response = client.PostAsJsonAsync(uri, obj);
            response.Wait();
            return response.Result;
        }

        public static HttpResponseMessage PostPlain(this HttpClient client, string uri, string s)
        {
            HttpContent content = new StringContent(s);
            Task<HttpResponseMessage> response = client.PostAsync(uri, content);
            response.Wait();
            return response.Result;
        }

        public static HttpResponseMessage Put(this HttpClient client, string uri, object obj)
        {
            Task<HttpResponseMessage> response = client.PutAsJsonAsync(uri, obj);
            response.Wait();
            return response.Result;
        }

        public static HttpResponseMessage PutPlain(this HttpClient client, string uri, string s)
        {
            HttpContent content = new StringContent(s);
            Task<HttpResponseMessage> response = client.PutAsync(uri, content);
            response.Wait();
            return response.Result;
        }

        public static HttpResponseMessage Patch(this HttpClient client, string uri, object obj)
        {
            Task<HttpResponseMessage> response = client.PatchAsJsonAsync(uri, obj);
            response.Wait();
            return response.Result;
        }

        public static HttpResponseMessage PatchPlain(this HttpClient client, string uri, string s)
        {
            HttpContent content = new StringContent(s);
            Task<HttpResponseMessage> response = client.PatchAsync(uri, content);
            response.Wait();
            return response.Result;
        }

        public static HttpResponseMessage Delete(this HttpClient client, string uri)
        {
            Task<HttpResponseMessage> response = client.DeleteAsync(uri);
            response.Wait();
            return response.Result;
        }

        private static Task<HttpResponseMessage> PatchAsJsonAsync<T>(this HttpClient client, string requestUri, T value)
        {
            var content = new ObjectContent<T>(value, new JsonMediaTypeFormatter());
            var request = new HttpRequestMessage(new HttpMethod("PATCH"), requestUri) { Content = content };

            return client.SendAsync(request);
        }

        private static Task<HttpResponseMessage> PatchAsync(this HttpClient client, string requestUri, HttpContent content)
        {
            var request = new HttpRequestMessage(new HttpMethod("PATCH"), requestUri) { Content = content };

            return client.SendAsync(request);
        }
    }
}
