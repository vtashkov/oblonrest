﻿using Oblon.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Patterns.Contracts;
using System.Text.RegularExpressions;

namespace Oblon.RestServer.Tests
{
    class OblonEngineMock : OblonEngine
    {
        public Exception ExceptionToBeThrown { get; set; }

        public dynamic OutputObject { get; set; }

        public dynamic InputObject { get; set; }

        public string DeletionKey { get; set; }

        public dynamic Select(string key, SelectModifier modifier)
        {
            if (ExceptionToBeThrown != null)
                throw ExceptionToBeThrown;
            return OutputObject;
        }

        public long Insert(string key, IDictionary<string, object> obj)
        {
            if (ExceptionToBeThrown != null)
                throw ExceptionToBeThrown;
            InputObject = obj;
            return Convert.ToInt64(obj["id"]);
        }

        public void Update(string key, IDictionary<string, object> obj)
        {
            if (ExceptionToBeThrown != null)
                throw ExceptionToBeThrown;
            InputObject = obj;
        }

        public void PartiallyUpdate(string key, IDictionary<string, object> obj)
        {
            if (ExceptionToBeThrown != null)
                throw ExceptionToBeThrown;
            InputObject = obj;
        }

        public void Delete(string key)
        {
            if (ExceptionToBeThrown != null)
                throw ExceptionToBeThrown;
            DeletionKey = key;
        }
    }
}
