﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oblon.Engine;
using System.Diagnostics.CodeAnalysis;
using Oblon.RelationalDatabase;
using System.Dynamic;

namespace Oblon.RelationalEngine.Tests
{
    [TestClass]
    public class DeleteTests
    {
        private RelationalDatabaseServerMock database;
        private OblonEngine engine;
        private string collectionName;

        [TestInitialize]
        public void Initialize()
        {
            database = new RelationalDatabaseServerMock();
            engine = new OblonRelationalEngine(database);
            collectionName = "/collection1";            

            database.AddRow(new Row() { RowType = RowTypes.Collection, Key = collectionName, ValueType = ValueTypes.Integer, Value = 0 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 1, Key = collectionName + "/1" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/String", Value = "String" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/Decimal", Value = 1.1M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/Boolean", Value = true });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/Redundant", Value = "Redundant" });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 1, Key = collectionName + "/2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 8, Key = collectionName + "/2/id", Value = 2 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 8, Key = collectionName + "/2/String", Value = "String2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 8, Key = collectionName + "/2/Decimal", Value = 1.2M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 8, Key = collectionName + "/2/Boolean", Value = false });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 1, Key = collectionName + "/3" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 13, Key = collectionName + "/3/id", Value = 3 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 13, Key = collectionName + "/3/String", Value = "String3" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 13, Key = collectionName + "/3/Decimal", Value = 1.3M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 13, Key = collectionName + "/3/Boolean", Value = true });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 13, Key = collectionName + "/3/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 18, Key = collectionName + "/3/InnerObj/String", Value = "InnerObjString" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 18, Key = collectionName + "/3/InnerObj/Decimal", Value = 9.9M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 18, Key = collectionName + "/3/InnerObj/Boolean", Value = false });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 18, Key = collectionName + "/3/InnerObj/Level2InnerObject" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 22, Key = collectionName + "/3/InnerObj/Level2InnerObject/String", Value = "Level2InnerObject" });
            database.AddRow(new Row() { RowType = RowTypes.Collection, ParentID = 13, Key = collectionName + "/3/InnerCollection" });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 24, Key = collectionName + "/3/InnerCollection/1" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 25, Key = collectionName + "/3/InnerCollection/1/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 24, Key = collectionName + "/3/InnerCollection/2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 27, Key = collectionName + "/3/InnerCollection/2/id", Value = 2 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 27, Key = collectionName + "/3/InnerCollection/2/String", Value = "InnerCollectionObject" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 27, Key = collectionName + "/3/InnerCollection/2/Decimal", Value = 8.8M });
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void DeletingWithNullKeyThrowsException()
        {
            engine.Delete(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void DeletingWithEmptyCollectionNamePassesSilently()
        {
            engine.Delete("");
        }

        [TestMethod]
        public void DeletingWithNonExistentKeyPassesSilently()
        {
            engine.Delete("nonexistent");
            Assert.AreEqual(30, database.Rows.Count);
        }

        [TestMethod]
        public void DeletingNonExistingObjectPassesSilently()
        {
            engine.Delete(collectionName + "/-1");
            Assert.AreEqual(30, database.Rows.Count);
        }

        [TestMethod]
        public void DeletingPropertyDoesNothing()
        {
            engine.Delete(collectionName + "/2/id");
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/2"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/2/id"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/2/String"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/2/Decimal"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/2/Boolean"));
        }

        [TestMethod]
        public void DeletingObjectRemovesItEntirely()
        {
            engine.Delete(collectionName + "/2");
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/2"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/2/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/2/String"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/2/Decimal"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/2/Boolean"));
        }

        [TestMethod]
        public void DeletingObjectRemovesItsInnerObjects()
        {
            engine.Delete(collectionName + "/3");
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerObj"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerObj/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerObj/String"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerObj/Decimal"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerObj/Boolean"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerObj/Level2InnerObject"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerObj//Level2InnerObject/String"));
        }

        [TestMethod]
        public void DeletingObjectRemovesItsInnerCollections()
        {
            engine.Delete(collectionName + "/3");
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerCollection"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerCollection/1"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerCollection/1/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerCollection/2"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerCollection/2/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerCollection/2/String"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == collectionName + "/3/InnerCollection/2/Decimal"));
        }

        [TestMethod]
        public void DeletingNonExistingCollectionPassesSilently()
        {
            engine.Delete("nonexistent");
        }

        [TestMethod]
        public void DeletingCollectionRemovesItAndAllItsObjects()
        {
            engine.Delete(collectionName);
            Assert.AreEqual(0, database.Rows.Count);
        }
    }
}
