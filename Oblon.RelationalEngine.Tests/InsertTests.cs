﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oblon.RelationalDatabase;
using System.Dynamic;
using Oblon.Engine;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;
using System.Linq;

namespace Oblon.RelationalEngine.Tests
{
    [TestClass]
    public class InsertTests
    {
        private RelationalDatabaseServerMock database;
        private OblonEngine engine;
        private string collectionName;
        private dynamic newObject;

        [TestInitialize]
        public void Initialize()
        {
            database = new RelationalDatabaseServerMock();
            engine = new OblonRelationalEngine(database);
            collectionName = "/collection1";
            newObject = new ExpandoObject();
            newObject.id = 5;
            newObject.Name = "Name";
            newObject.DecAttr = 1.1M;
            newObject.BoolValue = true;
            newObject.NullValue = null;

            database.AddRow(new Row() { RowType = RowTypes.Collection, Key = collectionName, ValueType = ValueTypes.Integer, Value = 0 });            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void InsertingObjectWithNullCollectionNameThrowsException()
        {
            engine.Insert(null, new ExpandoObject());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void InsertingNullObjectThrowsException()
        {
            engine.Insert(collectionName, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void InsertingObjectWithEmptyCollectionNameThrowsException()
        {
            engine.Insert("", new ExpandoObject());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidResourceKey))]
        [ExcludeFromCodeCoverage]
        public void InsertingObjectWithOnlySlashCollectionNameThrowsException()
        {
            engine.Insert("/", new ExpandoObject());
        }

        [TestMethod]
        public void InsertingObjectReturnsObjectID()
        {
            var id = engine.Insert(collectionName, newObject);
            Assert.AreEqual(newObject.id, id);
        }

        [TestMethod]
        public void InsertingObjectDoesntCreateCollectionIfExists()
        {
            engine.Insert(collectionName, newObject);
            Assert.AreEqual(1, database.Rows.Where(r => r.Key == collectionName).Count());
        }

        [TestMethod]
        public void InsertingCreatesNewCollectionIfNotExists()
        {
            engine.Insert("/newCollection", newObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == "/newCollection"));
        }

        [TestMethod]
        public void InsertingCreatesObjectRowAndPropertyRows()
        {
            engine.Insert(collectionName, newObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + newObject.id));
            Assert.AreEqual(Convert.ToInt64(newObject.id), database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/id").Value);
            Assert.AreEqual(newObject.Name, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/Name").Value);
            Assert.AreEqual(newObject.DecAttr, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/DecAttr").Value);
            Assert.AreEqual(newObject.BoolValue, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/BoolValue").Value);
            Assert.AreEqual(newObject.NullValue, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/NullValue").Value);
        }

        [TestMethod]
        [ExpectedException(typeof(DuplicatedObjectIDException))]
        [ExcludeFromCodeCoverage]
        public void InsertingObjectWithExistingIDThrowsException()
        {
            engine.Insert(collectionName, newObject);
            engine.Insert(collectionName, newObject);
        }

        [TestMethod]
        public void InsertingObjectWithoutIDCreatesCorrectRowsWithGeneratedID()
        {
            (newObject as IDictionary<string, object>).Remove("id");
            var id = engine.Insert(collectionName, newObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + id));
            Assert.AreEqual(id, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + id + "/id").Value);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidResourceKey))]
        [ExcludeFromCodeCoverage]
        public void InsertingObjectWithinAnObjectThrowsException()
        {
            engine.Insert(collectionName, newObject);
            engine.Insert(collectionName + "/" + newObject.id, new ExpandoObject());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidResourceKey))]
        [ExcludeFromCodeCoverage]
        public void InsertingObjectInANotExistentObjectCollectionThrowsException() 
        {
            engine.Insert(collectionName + "/" + newObject.id + "/collection", new ExpandoObject());
        }

        [TestMethod]
        public void InsertingInnerObjectCreatesObjectRowAndPropertyRows()
        {
            newObject.innerObject = new ExpandoObject();
            newObject.innerObject.name = "innerObject";
            engine.Insert(collectionName, newObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + newObject.id + "/innerObject"));
            Assert.AreEqual(newObject.innerObject.name, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/innerObject/name").Value);
        }

        [TestMethod]
        public void InsertingLevel2InnerObjectCreatesObjectRowAndPropertyRows()
        {
            newObject.innerObject = new ExpandoObject();
            newObject.innerObject.level2InnerObject = new ExpandoObject();
            newObject.innerObject.level2InnerObject.name = "Level2InnerObject";
            engine.Insert(collectionName, newObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + newObject.id + "/innerObject/level2InnerObject"));
            Assert.AreEqual(newObject.innerObject.level2InnerObject.name, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/innerObject/level2InnerObject/name").Value);
        }

        [TestMethod]
        public void InsertingObjectsInInnerCollectionCreatesCorrectRows()
        {
            dynamic innerCollectionObject = new ExpandoObject();
            innerCollectionObject.name = "innerCollectionObject";
            dynamic innerCollectionObject2 = new ExpandoObject();
            innerCollectionObject2.name = "innerCollectionObject2";
            newObject.innerCollection = (new List<dynamic>() { innerCollectionObject, innerCollectionObject2 } as IEnumerable<dynamic>);
            engine.Insert(collectionName, newObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection"));
            Assert.AreEqual(database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id).ID, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection").ParentID);
            Assert.AreEqual(RowTypes.Collection, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection").RowType);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection/1"));
            Assert.AreEqual(innerCollectionObject.name, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection/1/name").Value);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection/2"));
            Assert.AreEqual(innerCollectionObject2.name, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection/2/name").Value);
        }

        [TestMethod]
        public void InsertingObjectInExistingInnerCollectionCreatesCorrectObjectRowsAndPropertyRows()
        {
            dynamic innerCollectionObject = new ExpandoObject();
            innerCollectionObject.name = "innerCollectionObject";
            newObject.innerCollection = (new List<dynamic>() { innerCollectionObject } as IEnumerable<dynamic>);
            engine.Insert(collectionName, newObject);
            dynamic newInnerCollectionObject = new ExpandoObject();
            newInnerCollectionObject.name = "newInnerCollectionObject";
            engine.Insert(collectionName + "/5/innerCollection", newInnerCollectionObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection/2"));
            Assert.AreEqual(newInnerCollectionObject.name, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection/2/name").Value);
        }

        [TestMethod]
        public void InsertingObjectInANewCollectionWithinExistingObjectCreatesCorrectRows()
        {            
            engine.Insert(collectionName, newObject);
            dynamic newInnerCollectionObject = new ExpandoObject();
            newInnerCollectionObject.name = "newInnerCollectionObject";
            engine.Insert(collectionName + "/5/innerCollection", newInnerCollectionObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection"));
            Assert.AreEqual(RowTypes.Collection, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection").RowType);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection/1"));
            Assert.AreEqual(newInnerCollectionObject.name, database.Rows.FirstOrDefault(r => r.Key == collectionName + "/" + newObject.id + "/innerCollection/1/name").Value);
        }
    }
}