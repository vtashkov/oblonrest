﻿using Oblon.InMemoryRelationalDatabase;
using Oblon.RelationalDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RelationalEngine.Tests
{
    class RelationalDatabaseServerMock : InMemoryRelationalDatabaseServer
    {
        public List<Row> Rows { get { return rowsByID.Values.ToList() ; } }
    }
}
