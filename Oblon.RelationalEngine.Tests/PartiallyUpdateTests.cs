﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oblon.Engine;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using Oblon.RelationalDatabase;
using System.Linq;
using System.Collections.Generic;

namespace Oblon.RelationalEngine.Tests
{
    [TestClass]
    public class PartiallyUpdateTests
    {
        private RelationalDatabaseServerMock database;
        private OblonEngine engine;
        private string objectKey;
        private dynamic changedObject;

        [TestInitialize]
        public void Initialize()
        {
            database = new RelationalDatabaseServerMock();
            engine = new OblonRelationalEngine(database);
            objectKey = "/collection1/1";
            changedObject = new ExpandoObject();
            changedObject.id = 1;
            changedObject.String = "Name";
            changedObject.Decimal = 5.5M;
            changedObject.Boolean = false;
            changedObject.Null = null;
            changedObject.NewProperty = "New";

            database.AddRow(new Row() { RowType = RowTypes.Collection, Key = "/collection1", ValueType = ValueTypes.Integer, Value = 0 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 1, Key = objectKey });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/String", Value = "String" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/Decimal", Value = 1.1M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/Boolean", Value = true });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/Redundant", Value = "Redundant" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/Null", Value = "Null" });
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void PartiallyUpdatingObjectWithNullKeyThrowsException()
        {
            engine.PartiallyUpdate(null, changedObject);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void PartiallyUpdatingObjectWithEmptyKeyThrowsException()
        {
            engine.PartiallyUpdate("", changedObject);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void PartiallyUpdatingNullObjectThrowsException()
        {
            engine.PartiallyUpdate(objectKey, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ResourceNotFound))]
        [ExcludeFromCodeCoverage]
        public void PartiallyUpdatingObjectWithNonExistentKeyThrowsException()
        {
            engine.PartiallyUpdate("nonexistent", changedObject);
        }

        [TestMethod]
        [ExpectedException(typeof(ResourceNotFound))]
        [ExcludeFromCodeCoverage]
        public void PartiallyUpdatingNonExistingObjectThrowsException()
        {
            changedObject.id = -1;
            engine.PartiallyUpdate("/collection1/-1", changedObject);
        }

        [TestMethod]
        [ExpectedException(typeof(ResourceNotFound))]
        [ExcludeFromCodeCoverage]
        public void PartiallyUpdatingExistingObjectPropertyThrowsException()
        {
            engine.Update("/collection1/" + changedObject.id + "/id", changedObject);
        }

        [TestMethod]
        public void PartiallyUpdatingObjectChangesAttributes()
        {
            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.AreEqual(Convert.ToInt64(changedObject.id), database.Rows.FirstOrDefault(r => r.Key == objectKey + "/id").Value);
            Assert.AreEqual(changedObject.String, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/String").Value);
            Assert.AreEqual(changedObject.Decimal, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/Decimal").Value);
            Assert.AreEqual(changedObject.Boolean, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/Boolean").Value);
            Assert.AreEqual(changedObject.Null, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/Null").Value);
        }

        [TestMethod]
        public void PartiallyUpdatingObjectDoNotRemovesOldAttributes()
        {
            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/Redundant"));
        }

        [TestMethod]
        public void PartiallyUpdatingObjectAddsNewAttributes()
        {
            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.AreEqual(changedObject.NewProperty, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/NewProperty").Value);
        }

        [TestMethod]
        public void PartiallyUpdatingObjectDoesNotRemoveOldInnerObjectAndItsAttributes()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/String"));
        }

        [TestMethod]
        public void PartiallyUpdatingObjectChangesInnerObjects()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "NewInnerObj";
            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.AreEqual(changedObject.InnerObj.String, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/String").Value);
        }

        [TestMethod]
        public void PartiallyUpdatingObjectAddsNewInnerObjects()
        {
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/String"));
            Assert.AreEqual(changedObject.InnerObj.String, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/String").Value);
        }

        [TestMethod]
        public void PartiallyUpdatingObjectRemovesOldAttributesFromInnerObjects()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/Redundant", Value = "Redundant" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/Redundant"));
        }

        [TestMethod]
        public void PartiallyUpdatingObjectAddsNewAttributesToInnerObjects()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            changedObject.InnerObj.NewString = "NewString";
            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/String"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/NewString"));
            Assert.AreEqual(changedObject.InnerObj.NewString, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/NewString").Value);
        }

        [TestMethod]
        public void PartiallyUpdatingInnerObjectChangesAttributes()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "NewInnerObj";
            engine.PartiallyUpdate(objectKey + "/InnerObj", changedObject.InnerObj);
            Assert.AreEqual(changedObject.InnerObj.String, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/String").Value);
        }

        [TestMethod]
        public void PartiallyUpdatingInnerObjectDoesNotRemoveOldAttributes()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/Redundant", Value = "Redundant" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            engine.PartiallyUpdate(objectKey + "/InnerObj", changedObject.InnerObj);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/Redundant"));
        }

        [TestMethod]
        public void PartiallyUpdatingInnerObjectAddsNewAttributes()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            changedObject.InnerObj.NewProperty = "NewProperty";
            engine.PartiallyUpdate(objectKey + "/InnerObj", changedObject.InnerObj);
            Assert.AreEqual(changedObject.InnerObj.NewProperty, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/NewProperty").Value);
        }

        [TestMethod]
        public void PartiallyUpdatingObjectDoesNotRemoveOldInnerCollection()
        {
            database.AddRow(new Row() { RowType = RowTypes.Collection, ParentID = 2, Key = objectKey + "/InnerCollection" });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 9, Key = objectKey + "/InnerCollection/1" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 10, Key = objectKey + "/InnerCollection/1/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 9, Key = objectKey + "/InnerCollection/2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 12, Key = objectKey + "/InnerCollection/2/id", Value = 2 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 12, Key = objectKey + "/InnerCollection/2/name", Value = "InnerCollectionObject" });

            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1/id"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/id"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/name"));
        }

        [TestMethod]
        public void PartiallyUpdatingObjectChangesInnerCollection()
        {
            database.AddRow(new Row() { RowType = RowTypes.Collection, ParentID = 2, Key = objectKey + "/InnerCollection" });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 9, Key = objectKey + "/InnerCollection/1" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 10, Key = objectKey + "/InnerCollection/1/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 9, Key = objectKey + "/InnerCollection/2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 12, Key = objectKey + "/InnerCollection/2/id", Value = 2 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 12, Key = objectKey + "/InnerCollection/2/name", Value = "InnerCollectionObject" });

            dynamic innerCollectionObject = new ExpandoObject();
            innerCollectionObject.id = (long)3;
            dynamic innerCollectionObject2 = new ExpandoObject();
            innerCollectionObject2.id = (long)4;
            innerCollectionObject2.name = "ChangedInnerCollectionObject";
            changedObject.InnerCollection = (new List<dynamic>() { innerCollectionObject, innerCollectionObject2 } as IEnumerable<dynamic>);

            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.AreEqual(innerCollectionObject.id, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerCollection/3/id").Value);
            Assert.AreEqual(innerCollectionObject2.id, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerCollection/4/id").Value);
            Assert.AreEqual(innerCollectionObject2.name, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerCollection/4/name").Value);
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/name"));
        }

        [TestMethod]
        public void PartiallyUpdatingObjectAddsNewInnerCollections()
        {
            dynamic innerCollectionObject = new ExpandoObject();
            dynamic innerCollectionObject2 = new ExpandoObject();
            innerCollectionObject2.name = "ChangedInnerCollectionObject";
            changedObject.InnerCollection = (new List<dynamic>() { innerCollectionObject, innerCollectionObject2 } as IEnumerable<dynamic>);
            engine.PartiallyUpdate(objectKey, changedObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1/id"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/id"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/name"));
        }
    }
}