﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oblon.RelationalDatabase;
using System.Dynamic;
using System.Diagnostics.CodeAnalysis;
using Oblon.Engine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Oblon.RelationalEngine.Tests
{
    [TestClass]
    public class SelectTests
    {
        private RelationalDatabaseServerMock database;
        private OblonEngine engine;
        private string collectionName;        

        [TestInitialize]
        public void Initialize()
        {
            database = new RelationalDatabaseServerMock();
            engine = new OblonRelationalEngine(database);
            collectionName = "/collection1";

            database.AddRow(new Row() { RowType = RowTypes.Collection, Key = collectionName, ValueType = ValueTypes.Integer, Value = 0 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 1, Key = collectionName + "/1" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/String", Value = "String" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/Decimal", Value = 1.1M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/Boolean", Value = true });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/Null", Value = null });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = collectionName + "/1/Redundant", Value = "Redundant" });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 1, Key = collectionName + "/2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = collectionName + "/2/id", Value = 2 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = collectionName + "/2/String", Value = "String2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = collectionName + "/2/Decimal", Value = 1.2M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = collectionName + "/2/Boolean", Value = false });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 1, Key = collectionName + "/3" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 14, Key = collectionName + "/3/id", Value = 3 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 14, Key = collectionName + "/3/String", Value = "String3" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 14, Key = collectionName + "/3/Decimal", Value = 1.3M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 14, Key = collectionName + "/3/Boolean", Value = true });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 14, Key = collectionName + "/3/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 19, Key = collectionName + "/3/InnerObj/String", Value = "InnerObjString" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 19, Key = collectionName + "/3/InnerObj/Decimal", Value = 9.9M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 19, Key = collectionName + "/3/InnerObj/Boolean", Value = false });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 19, Key = collectionName + "/3/InnerObj/Level2InnerObject" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 23, Key = collectionName + "/3/InnerObj/Level2InnerObject/String", Value = "Level2InnerObject" });
            database.AddRow(new Row() { RowType = RowTypes.Collection, ParentID = 14, Key = collectionName + "/3/InnerCollection" });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 25, Key = collectionName + "/3/InnerCollection/1" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 26, Key = collectionName + "/3/InnerCollection/1/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 25, Key = collectionName + "/3/InnerCollection/2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 28, Key = collectionName + "/3/InnerCollection/2/id", Value = 2 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 28, Key = collectionName + "/3/InnerCollection/2/String", Value = "InnerCollectionObject" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 28, Key = collectionName + "/3/InnerCollection/2/Decimal", Value = 8.8M });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 1, Key = collectionName + "/4" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 32, Key = collectionName + "/4/id", Value = 4 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 32, Key = collectionName + "/4/String", Value = "String" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 32, Key = collectionName + "/4/Decimal", Value = 1.15M });
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void SelectingWithNullKeyThrowsException()
        {
            engine.Select(null, new SelectModifier());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void SelectingWithNullModifierThrowsException()
        {
            engine.Select("", null);
        }

        [TestMethod]
        public void SelectingWithEmptyKeyReturnsNull()
        {
            Assert.IsNull(engine.Select("", new SelectModifier()));
        }

        [TestMethod]
        public void SelectingCollectionWithNonExistentCollectionNameReturnsNull()
        {
            Assert.IsNull(engine.Select("/nonexistent", new SelectModifier()));
        }

        [TestMethod]
        public void SelectingObjectWithNonExistentCollectionNameReturnsNull()
        {
            Assert.IsNull(engine.Select("/nonexistent/1", new SelectModifier()));
        }

        [TestMethod]
        public void SelectingNonExistentObjectReturnsNull()
        {
            Assert.IsNull(engine.Select(collectionName + "/-1", new SelectModifier()));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidResourceKey))]
        [ExcludeFromCodeCoverage]
        public void SelectingPropertyThrowsException()
        {
            engine.Select(collectionName + "/1/id", new SelectModifier());
        }

        [TestMethod]
        public void SelectingObjectReturnsItCorrectly()
        {
            dynamic obj = engine.Select(collectionName + "/1", new SelectModifier());
            Assert.AreEqual(1, obj.id);
            Assert.AreEqual("String", obj.String);
            Assert.AreEqual(1.1M, obj.Decimal);
            Assert.AreEqual(true, obj.Boolean);
            Assert.AreEqual(null, obj.Null);
        }

        [TestMethod]
        public void SelectingCollectionReturnsCorrectObjects()
        {
            var collection = engine.Select(collectionName, new SelectModifier()) as IEnumerable<dynamic>;
            Assert.AreEqual(4, collection.Count());
            Assert.AreEqual(1, collection.First().id);
            Assert.AreEqual("String", collection.First().String);
            Assert.AreEqual(1.1M, collection.First().Decimal);
            Assert.AreEqual(true, collection.First().Boolean);
            Assert.AreEqual(null, collection.First().Null);
            Assert.AreEqual(2, collection.ElementAt(1).id);
            Assert.AreEqual("String2", collection.ElementAt(1).String);
            Assert.AreEqual(1.2M, collection.ElementAt(1).Decimal);
            Assert.AreEqual(false, collection.ElementAt(1).Boolean);
            Assert.AreEqual(3, collection.ElementAt(2).id);
            Assert.AreEqual("String3", collection.ElementAt(2).String);
            Assert.AreEqual(1.3M, collection.ElementAt(2).Decimal);
            Assert.AreEqual(true, collection.ElementAt(2).Boolean);
            Assert.AreEqual(4, collection.ElementAt(3).id);
            Assert.AreEqual(1.15M, collection.ElementAt(3).Decimal);
        }

        [TestMethod]
        public void SelectingObjectWithInnerObjectDoesNotReturnIt()
        {
            dynamic obj = engine.Select(collectionName + "/3", new SelectModifier());
            Assert.IsFalse((obj as IDictionary<string, object>).ContainsKey("InnerObj"));
        }

        [TestMethod]
        public void SelectingNonExistentInnerObjectObjectReturnsNull()
        {
            Assert.IsNull(engine.Select(collectionName + "/3/NonExistentObj", new SelectModifier()));
        }

        [TestMethod]
        public void SelectingInnerObjectReturnsItCorrectly()
        {
            dynamic obj = engine.Select(collectionName + "/3/InnerObj", new SelectModifier());
            Assert.AreEqual("InnerObjString", obj.String);
            Assert.AreEqual(9.9M, obj.Decimal);
            Assert.AreEqual(false, obj.Boolean);
        }

        [TestMethod]
        public void SelectingLevel2InnerObjectReturnsItCorrectly()
        {
            dynamic obj = engine.Select(collectionName + "/3/InnerObj/Level2InnerObject", new SelectModifier());
            Assert.AreEqual("Level2InnerObject", obj.String);
        }

        [TestMethod]
        public void SelectingInnerCollectionObjectReturnsItCorrectly()
        {
            dynamic obj = engine.Select(collectionName + "/3/InnerCollection/2", new SelectModifier());
            Assert.AreEqual(2, obj.id);
            Assert.AreEqual("InnerCollectionObject", obj.String);
            Assert.AreEqual(8.8M, obj.Decimal);
        }

        [TestMethod]
        public void SelectingInnerCollectionReturnsCorrectObjects()
        {
            var collection = engine.Select(collectionName + "/3/InnerCollection", new SelectModifier()) as IEnumerable<dynamic>;
            Assert.AreEqual(1, collection.ElementAt(0).id);
            Assert.AreEqual(2, collection.ElementAt(1).id);
            Assert.AreEqual("InnerCollectionObject", collection.ElementAt(1).String);
            Assert.AreEqual(8.8M, collection.ElementAt(1).Decimal);
        }

        [TestMethod]
        public void SelectingObjectWithNonExistentFieldsModifierReturnsOnlyExisting()
        {
            var modifier = new SelectModifier();
            modifier.Fields = new string[] { "String", "NonExistent" };
            dynamic obj = engine.Select(collectionName + "/1", modifier);
            Assert.AreEqual("String", obj.String);
            Assert.IsFalse((obj as IDictionary<string, object>).ContainsKey("NonExistent"));
        }

        [TestMethod]
        public void SelectingObjectWithFieldsModifierReturnsOnlyThem()
        {
            var modifier = new SelectModifier();
            modifier.Fields = new string[] { "String", "Decimal" };
            dynamic obj = engine.Select(collectionName + "/1", modifier);
            Assert.IsFalse((obj as IDictionary<string, object>).ContainsKey("id"));
            Assert.AreEqual("String", obj.String);
            Assert.AreEqual(1.1M, obj.Decimal);
            Assert.IsFalse((obj as IDictionary<string, object>).ContainsKey("Boolean"));
            Assert.IsFalse((obj as IDictionary<string, object>).ContainsKey("Null"));
        }

        [TestMethod]
        public void SelectingObjectWithInnerObjectInFieldsModifierReturnsIt()
        {
            var modifier = new SelectModifier();
            modifier.Fields = new string[] { "String", "InnerObj" };
            dynamic obj = engine.Select(collectionName + "/3", modifier);
            Assert.AreEqual("String3", obj.String);
            Assert.AreEqual("InnerObjString", obj.InnerObj.String);
            Assert.AreEqual(9.9M, obj.InnerObj.Decimal);
            Assert.AreEqual(false, obj.InnerObj.Boolean);
            Assert.AreEqual("Level2InnerObject", obj.InnerObj.Level2InnerObject.String);
        }

        [TestMethod]
        public void SelectingObjectWithInnerCollectionInFieldsModifierReturnsIt()
        {
            var modifier = new SelectModifier();
            modifier.Fields = new string[] { "String", "InnerCollection" };
            dynamic obj = engine.Select(collectionName + "/3", modifier);
            Assert.AreEqual("String3", obj.String);
            var collection = obj.InnerCollection as IEnumerable<dynamic>;
            Assert.AreEqual(1, collection.ElementAt(0).id);
            Assert.AreEqual(2, collection.ElementAt(1).id);
            Assert.AreEqual("InnerCollectionObject", collection.ElementAt(1).String);
            Assert.AreEqual(8.8M, collection.ElementAt(1).Decimal);            
        }

        [TestMethod]
        public void SelectingObjectWithNonExistentExpandModifierReturnsTheCorrectOnes()
        {
            var modifier = new SelectModifier();
            modifier.Expand = new string[] { "NonExistent" };
            dynamic obj = engine.Select(collectionName + "/1", modifier);
            Assert.AreEqual(1, obj.id);
            Assert.AreEqual("String", obj.String);
            Assert.AreEqual(1.1M, obj.Decimal);
            Assert.AreEqual(true, obj.Boolean);
            Assert.AreEqual(null, obj.Null);
        }

        [TestMethod]
        public void SelectingObjectWithInnerObjectInExpandModifierReturnsIt()
        {
            var modifier = new SelectModifier();
            modifier.Expand = new string[] { "InnerObj" };
            dynamic obj = engine.Select(collectionName + "/3", modifier);
            Assert.AreEqual(3, obj.id);
            Assert.AreEqual("String3", obj.String);
            Assert.AreEqual(1.3M, obj.Decimal);
            Assert.AreEqual(true, obj.Boolean);
            Assert.AreEqual("InnerObjString", obj.InnerObj.String);
            Assert.AreEqual(9.9M, obj.InnerObj.Decimal);
            Assert.AreEqual(false, obj.InnerObj.Boolean);
            Assert.AreEqual("Level2InnerObject", obj.InnerObj.Level2InnerObject.String);
        }

        [TestMethod]
        public void SelectingObjectWithInnerCollectionInExpandModifierReturnsIt()
        {
            var modifier = new SelectModifier();
            modifier.Expand = new string[] { "InnerCollection" };
            dynamic obj = engine.Select(collectionName + "/3", modifier);
            Assert.AreEqual(3, obj.id);
            Assert.AreEqual("String3", obj.String);
            Assert.AreEqual(1.3M, obj.Decimal);
            Assert.AreEqual(true, obj.Boolean);
            var collection = obj.InnerCollection as IEnumerable<dynamic>;
            Assert.AreEqual(1, collection.ElementAt(0).id);
            Assert.AreEqual(2, collection.ElementAt(1).id);
            Assert.AreEqual("InnerCollectionObject", collection.ElementAt(1).String);
            Assert.AreEqual(8.8M, collection.ElementAt(1).Decimal);
        }

        [TestMethod]
        public void SelectingObjectWithInnerObjectInExpandAndSomeFieldsInModifierReturnsCorrectOnes()
        {
            var modifier = new SelectModifier();
            modifier.Fields = new string[] { "String", "Decimal" };
            modifier.Expand = new string[] { "InnerObj" };
            dynamic obj = engine.Select(collectionName + "/3", modifier);
            Assert.IsFalse((obj as IDictionary<string, object>).ContainsKey("id"));            
            Assert.AreEqual("String3", obj.String);
            Assert.AreEqual(1.3M, obj.Decimal);
            Assert.IsFalse((obj as IDictionary<string, object>).ContainsKey("Boolean"));                        
            Assert.AreEqual("InnerObjString", obj.InnerObj.String);
            Assert.AreEqual(9.9M, obj.InnerObj.Decimal);
            Assert.AreEqual(false, obj.InnerObj.Boolean);
            Assert.AreEqual("Level2InnerObject", obj.InnerObj.Level2InnerObject.String);
        }

        [TestMethod]
        public void SelectingObjectWithFullModifierReturnsFullObject()
        {
            var modifier = new SelectModifier();
            modifier.Full = true;
            dynamic obj = engine.Select(collectionName + "/3", modifier);
            Assert.AreEqual(3, obj.id);
            Assert.AreEqual("String3", obj.String);
            Assert.AreEqual(1.3M, obj.Decimal);
            Assert.AreEqual(true, obj.Boolean);
            Assert.AreEqual("InnerObjString", obj.InnerObj.String);
            Assert.AreEqual(9.9M, obj.InnerObj.Decimal);
            Assert.AreEqual(false, obj.InnerObj.Boolean);
            Assert.AreEqual("Level2InnerObject", obj.InnerObj.Level2InnerObject.String);
            var collection = obj.InnerCollection as IEnumerable<dynamic>;
            Assert.AreEqual(1, collection.ElementAt(0).id);
            Assert.AreEqual(2, collection.ElementAt(1).id);
            Assert.AreEqual("InnerCollectionObject", collection.ElementAt(1).String);
            Assert.AreEqual(8.8M, collection.ElementAt(1).Decimal);
        }

        [TestMethod]
        public void SelectingObjectWithOrderModifierReturnsTheSameObject()
        {
            var modifier = new SelectModifier();
            modifier.LinqQuery = "$orderby=[id] desc";
            dynamic obj = engine.Select(collectionName + "/1", new SelectModifier());
            Assert.AreEqual(1, obj.id);
            Assert.AreEqual("String", obj.String);
            Assert.AreEqual(1.1M, obj.Decimal);
            Assert.AreEqual(true, obj.Boolean);
            Assert.AreEqual(null, obj.Null);
        }

        [TestMethod]
        public void SelectingCollectionWithOneAscendingOrderModifierReturnsItSorted()
        {
            var modifier = new SelectModifier();
            modifier.LinqQuery = "$orderby=[Decimal]";
            var collection = engine.Select(collectionName, modifier) as IEnumerable<dynamic>;                       
            Assert.AreEqual(1.1M, collection.First().Decimal);
            Assert.AreEqual(1.15M, collection.ElementAt(1).Decimal);
            Assert.AreEqual(1.2M, collection.ElementAt(2).Decimal);
            Assert.AreEqual(1.3M, collection.ElementAt(3).Decimal);
        }

        [TestMethod]
        public void SelectingCollectionWithOneDescendingOrderModifierReturnsItSorted()
        {
            var modifier = new SelectModifier();
            modifier.LinqQuery = "$orderby=[Decimal] desc";
            var collection = engine.Select(collectionName, modifier) as IEnumerable<dynamic>;
            Assert.AreEqual(1.3M, collection.First().Decimal);
            Assert.AreEqual(1.2M, collection.ElementAt(1).Decimal);
            Assert.AreEqual(1.15M, collection.ElementAt(2).Decimal);
            Assert.AreEqual(1.1M, collection.ElementAt(3).Decimal);
        }

        [TestMethod]
        public void SelectingCollectionWithTwoOrderModifierReturnsItSorted()
        {
            var modifier = new SelectModifier();
            modifier.LinqQuery = "$orderby=[String] asc,[Decimal] desc";
            var collection = engine.Select(collectionName, modifier) as IEnumerable<dynamic>;
            Assert.AreEqual(1.15M, collection.First().Decimal);
            Assert.AreEqual(1.1M, collection.ElementAt(1).Decimal);
            Assert.AreEqual(1.2M, collection.ElementAt(2).Decimal);
            Assert.AreEqual(1.3M, collection.ElementAt(3).Decimal);
        }

        [TestMethod]
        public void SelectingCollectionWithThreeOrderModifierReturnsItSorted()
        {
            var modifier = new SelectModifier();
            modifier.LinqQuery = "$orderby=[String] desc,[Decimal] asc";
            var collection = engine.Select(collectionName, modifier) as IEnumerable<dynamic>;
            Assert.AreEqual(1.3M, collection.First().Decimal);
            Assert.AreEqual(1.2M, collection.ElementAt(1).Decimal);
            Assert.AreEqual(1.1M, collection.ElementAt(2).Decimal);
            Assert.AreEqual(1.15M, collection.ElementAt(3).Decimal);
        }

        [TestMethod]
        public void SelectingCollectionWithFilterReturnsItFiltered()
        {
            var modifier = new SelectModifier();
            modifier.LinqQuery = "$filter=[id] ge 2L";
            var collection = engine.Select(collectionName, modifier) as IEnumerable<dynamic>;
            Assert.AreEqual(3, collection.Count());
            Assert.AreEqual(1.2M, collection.First().Decimal);
        }
    }
}