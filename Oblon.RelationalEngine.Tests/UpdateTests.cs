﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Dynamic;
using Oblon.Engine;
using Oblon.RelationalDatabase;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Collections.Generic;

namespace Oblon.RelationalEngine.Tests
{
    [TestClass]
    public class UpdateTests
    {
        private RelationalDatabaseServerMock database;
        private OblonEngine engine;
        private string objectKey;
        private dynamic newObject;
        private dynamic changedObject;

        [TestInitialize]
        public void Initialize()
        {
            database = new RelationalDatabaseServerMock();
            engine = new OblonRelationalEngine(database);
            objectKey = "/collection1/1";
            newObject = new ExpandoObject();
            newObject.id = 5;
            changedObject = new ExpandoObject();
            changedObject.id = 1;
            changedObject.String = "Name";
            changedObject.Decimal = 5.5M;
            changedObject.Boolean = false;
            changedObject.Null = null;
            changedObject.NewProperty = "New";

            database.AddRow(new Row() { RowType = RowTypes.Collection, Key = "/collection1", ValueType = ValueTypes.Integer, Value = 0 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 1, Key = objectKey });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/String", Value = "String" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/Decimal", Value = 1.1M });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/Boolean", Value = true });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/Redundant", Value = "Redundant" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 2, Key = objectKey + "/Null", Value = "Null" });
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void UpdatingObjectWithNullKeyThrowsException()
        {
            engine.Update(null, changedObject);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void UpdatingObjectWithEmptyKeyThrowsException()
        {
            engine.Update("", changedObject);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void UpdatingNullObjectThrowsException()
        {
            engine.Update(objectKey, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ResourceNotFound))]
        [ExcludeFromCodeCoverage]
        public void UpdatingObjectWithNonExistentKeyThrowsException()
        {
            engine.Update("nonexistent", changedObject);
        }

        [TestMethod]
        [ExpectedException(typeof(ResourceNotFound))]
        [ExcludeFromCodeCoverage]
        public void UpdatingNonExistingObjectThrowsException()
        {
            changedObject.id = -1;
            engine.Update("/collection1/-1", changedObject);
        }

        [TestMethod]
        [ExpectedException(typeof(ResourceNotFound))]
        [ExcludeFromCodeCoverage]
        public void UpdatingExistingObjectPropertyThrowsException()
        {
            engine.Update("/collection1/" + changedObject.id + "/id", changedObject);
        }

        [TestMethod]
        public void UpdatingObjectChangesAttributes()
        {
            engine.Update(objectKey, changedObject);
            Assert.AreEqual(Convert.ToInt64(changedObject.id), database.Rows.FirstOrDefault(r => r.Key == objectKey + "/id").Value);
            Assert.AreEqual(changedObject.String, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/String").Value);
            Assert.AreEqual(changedObject.Decimal, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/Decimal").Value);
            Assert.AreEqual(changedObject.Boolean, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/Boolean").Value);
            Assert.AreEqual(changedObject.Null, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/Null").Value);
        }

        [TestMethod]
        public void UpdatingObjectRemovesOldAttributes()
        {
            engine.Update(objectKey, changedObject);
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/Redundant"));
        }

        [TestMethod]
        public void UpdatingObjectAddsNewAttributes()
        {
            engine.Update(objectKey, changedObject);
            Assert.AreEqual(changedObject.NewProperty, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/NewProperty").Value);
        }

        [TestMethod]
        public void UpdatingObjectRemovesOldInnerObjectAndItsAttributes()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            engine.Update(objectKey, changedObject);
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/String"));
        }

        [TestMethod]
        public void UpdatingObjectChangesInnerObjects()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "NewInnerObj";
            engine.Update(objectKey, changedObject);
            Assert.AreEqual(changedObject.InnerObj.String, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/String").Value);
        }

        [TestMethod]
        public void UpdatingObjectAddsNewInnerObjects()
        {
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            engine.Update(objectKey, changedObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/String"));
            Assert.AreEqual(changedObject.InnerObj.String, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/String").Value);
        }

        [TestMethod]
        public void UpdatingObjectRemovesOldAttributesFromInnerObjects()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/Redundant", Value = "Redundant" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            engine.Update(objectKey, changedObject);
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/Redundant"));
        }

        [TestMethod]
        public void UpdatingObjectAddsNewAttributesToInnerObjects()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            changedObject.InnerObj.NewString = "NewString";
            engine.Update(objectKey, changedObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/String"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/NewString"));
            Assert.AreEqual(changedObject.InnerObj.NewString, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/NewString").Value);
        }

        [TestMethod]
        public void UpdatingInnerObjectChangesAttributes()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "NewInnerObj";
            engine.Update(objectKey + "/InnerObj", changedObject.InnerObj);
            Assert.AreEqual(changedObject.InnerObj.String, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/String").Value);
        }

        [TestMethod]
        public void UpdatingInnerObjectRemovesOldAttributes()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/Redundant", Value = "Redundant" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            engine.Update(objectKey + "/InnerObj", changedObject.InnerObj);
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerObj/Redundant"));
        }

        [TestMethod]
        public void UpdatingInnerObjectAddsNewAttributes()
        {
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 2, Key = objectKey + "/InnerObj" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 9, Key = objectKey + "/InnerObj/String", Value = "InnerObj" });
            changedObject.InnerObj = new ExpandoObject();
            changedObject.InnerObj.String = "InnerObj";
            changedObject.InnerObj.NewProperty = "NewProperty";
            engine.Update(objectKey + "/InnerObj", changedObject.InnerObj);
            Assert.AreEqual(changedObject.InnerObj.NewProperty, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerObj/NewProperty").Value);
        }

        [TestMethod]
        public void UpdatingObjectRemovesOldInnerCollection()
        {
            database.AddRow(new Row() { RowType = RowTypes.Collection, ParentID = 2, Key = objectKey + "/InnerCollection" });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 9, Key = objectKey + "/InnerCollection/1" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 10, Key = objectKey + "/InnerCollection/1/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 9, Key = objectKey + "/InnerCollection/2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 12, Key = objectKey + "/InnerCollection/2/id", Value = 2 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 12, Key = objectKey + "/InnerCollection/2/name", Value = "InnerCollectionObject" });

            engine.Update(objectKey, changedObject);
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/name"));
        }

        [TestMethod]
        public void UpdatingObjectChangesInnerCollection()
        {
            database.AddRow(new Row() { RowType = RowTypes.Collection, ParentID = 2, Key = objectKey + "/InnerCollection" });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 9, Key = objectKey + "/InnerCollection/1" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 10, Key = objectKey + "/InnerCollection/1/id", Value = 1 });
            database.AddRow(new Row() { RowType = RowTypes.Object, ParentID = 9, Key = objectKey + "/InnerCollection/2" });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 12, Key = objectKey + "/InnerCollection/2/id", Value = 2 });
            database.AddRow(new Row() { RowType = RowTypes.Property, ParentID = 12, Key = objectKey + "/InnerCollection/2/name", Value = "InnerCollectionObject" });

            dynamic innerCollectionObject = new ExpandoObject();
            innerCollectionObject.id = (long)3;
            dynamic innerCollectionObject2 = new ExpandoObject();
            innerCollectionObject2.id = (long)4;
            innerCollectionObject2.name = "ChangedInnerCollectionObject";
            changedObject.InnerCollection = (new List<dynamic>() { innerCollectionObject, innerCollectionObject2 } as IEnumerable<dynamic>);

            engine.Update(objectKey, changedObject);
            Assert.AreEqual(innerCollectionObject.id, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerCollection/3/id").Value);
            Assert.AreEqual(innerCollectionObject2.id, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerCollection/4/id").Value);
            Assert.AreEqual(innerCollectionObject2.name, database.Rows.FirstOrDefault(r => r.Key == objectKey + "/InnerCollection/4/name").Value);
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/id"));
            Assert.IsFalse(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/name"));
        }

        [TestMethod]
        public void UpdatingObjectAddsNewInnerCollections()
        {
            dynamic innerCollectionObject = new ExpandoObject();
            dynamic innerCollectionObject2 = new ExpandoObject();
            innerCollectionObject2.name = "ChangedInnerCollectionObject";
            changedObject.InnerCollection = (new List<dynamic>() { innerCollectionObject, innerCollectionObject2 } as IEnumerable<dynamic>);
            engine.Update(objectKey, changedObject);
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/1/id"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/id"));
            Assert.IsTrue(database.Rows.Exists(r => r.Key == objectKey + "/InnerCollection/2/name"));
        }
    }
}
