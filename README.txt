Oblon Server 0.3.1 / 2.11.2015 - Universal REST Gateway

USAGE: 
 oblon [configuration_file]

LIMITATIONS:
 - only in-memory database (no persistence)
 - no error messages

 CHANGE LOG:

  version 0.1.1:
   - PUT and PATCH requests are changed to use the object id from the URL, instead of body content

  version 0.1.2:
   - In-memory database performance refactored

  version 0.1.3:
   - automatic generation of object id is added
   
   version 0.2:
   - inner (nested) objects and collections added

   version 0.3:
   - configuration file (oblon.config) added, instead of command-line arguments
   - basic authentication added
   - CORS enabled
   - resource-level authorization added

   version 0.3.1
   - ability to process gzip encoding added to the response and the request