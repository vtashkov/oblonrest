﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RelationalDatabase
{
    public interface RelationalDatabaseServer
    {
        Row GetRowByID(long id);

        Row GetRowByKey(string key);

        Row GetRowByKeyAndType(string key, RowTypes type);

        IEnumerable<Row> GetRowsByParentID(long parentID);

        long AddRow(Row row);

        void UpdateRowByID(long id, object value);

        long GetIncrementedRowValueByID(long id);

        void DeleteRowByID(long id);
    }
}
