﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RelationalDatabase
{
    public class CollectionRow : Row
    {
        public CollectionRow(string key)
        {
            Key = key;
            RowType = RowTypes.Collection;
            ValueType = ValueTypes.Integer;
            Value = 0;
        }
    }
}
