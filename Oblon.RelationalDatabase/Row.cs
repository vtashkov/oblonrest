﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oblon.RelationalDatabase
{
    public class Row
    {
        private static Dictionary<ValueTypes, Func<string, object>> ConversionToObjectMap = new Dictionary<ValueTypes, Func<string, object>>() 
                                                                                            {  
                                                                                                { ValueTypes.Alphanumeric, s => Convert.ToString(s) },
                                                                                                { ValueTypes.Integer, s => Convert.ToInt64(s) },
                                                                                                { ValueTypes.Decimal, s => Convert.ToDecimal(s) },
                                                                                                { ValueTypes.Boolean, s => Convert.ToBoolean(s) },
                                                                                            };
        private static Dictionary<Type, ValueTypes> ConversionToTypeMap = new Dictionary<Type, ValueTypes>()
                                                                          {
                                                                              { typeof(string), ValueTypes.Alphanumeric },
                                                                              { typeof(byte), ValueTypes.Integer },
                                                                              { typeof(short), ValueTypes.Integer },
                                                                              { typeof(int), ValueTypes.Integer },
                                                                              { typeof(long), ValueTypes.Integer },
                                                                              { typeof(decimal), ValueTypes.Decimal },
                                                                              { typeof(float), ValueTypes.Decimal },
                                                                              { typeof(double), ValueTypes.Decimal },
                                                                              { typeof(bool), ValueTypes.Boolean }
                                                                          };

        public long? ID { get; set; }

        public RowTypes RowType { get; set; }

        public long? ParentID { get; set; }

        private string key;
        public string Key 
        {
            get { return key; }
            set { key = value; LastKey = Key.Split('/').Last(); } 
        }

        public string LastKey { get; private set; }

        public string StringValue { get; set; }

        public ValueTypes? ValueType { get; set; }

        public object Value
        { 
            get 
            {
                return ValueType.HasValue ? ConversionToObjectMap[ValueType.Value](StringValue) : null; 
            }

            set             
            {
                ValueType = value != null ? ConversionToTypeMap[value.GetType()] : (ValueTypes?)null;
                StringValue = value != null ? value.ToString() : null;
            }
        }
    }
}
